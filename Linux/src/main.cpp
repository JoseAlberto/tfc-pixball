#define UNUSED_VARIABLE(x) (void)x

#include "GameManager.h"
#include "IAManager.h"
#include "RecordManager.h"
#include "IntroState.h"
#include "LevelSelectionState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "MenuState.h"
#include "EndState.h"

#include <iostream>

using namespace std;

int main () {

    GameManager* game = new GameManager();
    IAManager* ias = new IAManager();
    RecordManager* rm = new RecordManager();
    IntroState* introState = new IntroState();
    LevelSelectionState * levelselection = new LevelSelectionState();
    PlayState* playState = new PlayState();
    PauseState* pauseState = new PauseState();
    MenuState* menuState = new MenuState();
    EndState* endState = new EndState();

    UNUSED_VARIABLE(ias);
    UNUSED_VARIABLE(rm);
    UNUSED_VARIABLE(introState);
    UNUSED_VARIABLE(levelselection);
    UNUSED_VARIABLE(playState);
    UNUSED_VARIABLE(pauseState);
    UNUSED_VARIABLE(menuState);
    UNUSED_VARIABLE(endState);
    
    try {
      game->start(IntroState::getSingletonPtr());
    } catch (Ogre::Exception& e) {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }//Fin try-catch
  
    delete game;
  
    return 0;
}//Fin main
