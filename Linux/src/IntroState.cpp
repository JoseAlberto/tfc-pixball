#include "IntroState.h"

#include "MenuState.h"
#include "RecordManager.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void IntroState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

    _camera = _sceneMgr->createCamera("MainCamera");
    _camera->setPosition(Ogre::Vector3(0, 3, 10));
    _camera->lookAt(Ogre::Vector3(0, 0, 0));
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));

    double width = _viewport->getActualWidth();
    double height = _viewport->getActualHeight();
    _camera->setAspectRatio(width / height);

    std::cout << "Cargando Records..." << std::endl;
    RecordManager::getSingletonPtr()->loadRecords();

    _pTrackManager=TrackManager::getSingletonPtr();

    /* Reinicio de semilla */
  	srand(time(NULL));
    int aleatorio = (unsigned)rand() % 2;

    switch(aleatorio){
    case 0:
        _mainTrack=_pTrackManager->load("POL-fly-hunter-short.wav");
        break;
    case 1:
    	_mainTrack=_pTrackManager->load("POL-castle-rooms-short.wav");
    	break;
    default:
    	_mainTrack=_pTrackManager->load("POL-fly-hunter-short.wav");
    	break;
    }//FIn switch

    _time = 0.0;

    createOverlay();
    _exitGame = _scene = false;
}//Fin enter

void IntroState::createScene(){
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *node = NULL;

    _camera->setPosition(Ogre::Vector3(0, 0, 15));
    _camera->lookAt(Ogre::Vector3(0, 0, 0));

    /* Titulo */
    node = _sceneMgr->getRootSceneNode()->
            createChildSceneNode("Title", Ogre::Vector3(0, 0, 0));
    ent = _sceneMgr->createEntity("Title.mesh");
    node->attachObject(ent);

    /* Press */
    node = _sceneMgr->getRootSceneNode()->
            createChildSceneNode("Press_Start", Ogre::Vector3(0, -3, 0));
    ent = _sceneMgr->createEntity("Press.mesh");
    node->attachObject(ent);

    /* Background */
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton(
        ).create("Intro_Background", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("stars.jpg");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

    // Create background rectangle covering the whole screen
    _rect = new Ogre::Rectangle2D(true);
    _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    _rect->setMaterial("Intro_Background");

    // Render the background before everything else
    _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

    // Use infinite AAB to always stay visible
    Ogre::AxisAlignedBox aabInf;
    aabInf.setInfinite();
    _rect->setBoundingBox(aabInf);

    // Attach background to the scene
    node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Intro_Background");
    node->attachObject(_rect);

    // Example of background scrolling
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(-0.15, 0.0);

    /* Sombras */
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.75, 0.75, 0.75));

    /* Iluminacion */
    node = _sceneMgr->createSceneNode("LightingNode");
    Ogre::Light *light = _sceneMgr->createLight("Light");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(-0.5, -1, -1));
    node->attachObject(light);

    _mainTrack->play();
}//Fin if

void IntroState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    _ovintro = _overlayManager->getByName("Intro");
    _ov_bullet = _overlayManager->getOverlayElement("imgBullet");
    _ov_ogre = _overlayManager->getOverlayElement("imgOgre");

    _ov_bullet->hide();
    _ov_ogre->hide();
    _ovintro->show();
}//Fin createOverlay

void IntroState::exit() {
    delete _rect;

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void IntroState::pause () {}

void IntroState::resume () {}

bool IntroState::frameStarted (const Ogre::FrameEvent& evt) {

    if(!_scene){
        _time += evt.timeSinceLastFrame;

        if(_time >= 0.5 && _time < 2.5 && !_ov_ogre->isVisible()){
            _ov_ogre->show();
        } else if(_time >= 2.5 && _time < 4.5 && !_ov_bullet->isVisible()){
            _ov_ogre->hide();
            _ov_bullet->show();
        } else if(_time >= 4.5 && _ov_bullet->isVisible()){
            _ov_bullet->hide();
            
            createScene();
            _scene = true;
        }//Fin if-else
    }//Fin if

    return true;
}//Fin frameStarted

bool IntroState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void IntroState::keyPressed (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_SPACE) {
        if(_scene){
            changeState(MenuState::getSingletonPtr());
        } else if(_ov_ogre->isVisible()){
            _ov_ogre->hide();
            _ov_bullet->show();
            _time = 2.5;
        } else if(_ov_bullet->isVisible()){
            _ov_bullet->hide();
            _ovintro->hide();

            if(!_scene){
                createScene();
                _scene = true;
            }//Fin if-else
        }//Fin if
    }//Fin if
}//Fin keyPressed

void IntroState::keyReleased (const OIS::KeyEvent &e ) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }//Fin if
}//Fin keyReleased

void IntroState::mouseMoved (const OIS::MouseEvent &e) {}

void IntroState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void IntroState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

IntroState* IntroState::getSingletonPtr () {
    return msSingleton;
}//Fin getSingletonPtr

IntroState& IntroState::getSingleton () { 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton
