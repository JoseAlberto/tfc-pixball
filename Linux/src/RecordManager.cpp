#include "RecordManager.h"

template<> RecordManager* Ogre::Singleton<RecordManager>::msSingleton = 0;

RecordManager::RecordManager(){
	_docName = "Records.txt";
}//Fin contructor

RecordManager::~RecordManager(){
	_records.clear();
}//Fin destructor

void RecordManager::saveRecords(){
	std::ofstream file(_docName.c_str());
	std::map<int, Record>::iterator last_iter = _records.end();

	if(_records.size() > 0){
		last_iter--;
	}//Fin if
	
    if (file.is_open()){
        for (std::map<int, Record>::iterator it = _records.begin(); 
            it != _records.end(); ++it){
            file << it->first 
        		 << ":" << it->second.getTime()
                 << ":" << it->second.getPoints();
            if(it != last_iter){
            	file << '\n';
            }//Fin if
        }//Fin for
        file.close();
    } else {
        std::cout << "Error al escribir archivo" << std::endl;
    }//Fin if-else
}//Fin saveRecords

void RecordManager::loadRecords(){
	Record *rec;
	std::string str;
	Ogre::StringVector vct;
    std::ifstream file(_docName.c_str());

    if (file.is_open()){
        while (getline(file, str)){
        	/* Hacemos split por ":" para obtener la posicion, el nombre del jugador y la puntuacion */
            vct = Ogre::StringUtil::split(str.c_str(), ":");

            rec = new Record();
            /* Agregamos el planeta al record */
        	rec->setPlanet(Ogre::StringConverter::parseInt(vct[0]));
        	/* Agregamos el tiempo al record */
        	rec->setTime(Ogre::StringConverter::parseReal(vct[1]));
        	/* Agregamos la puntuacion al record */
        	rec->setPoints(Ogre::StringConverter::parseInt(vct[2]));

        	/* Agregamos el record al map */
        	addRecord(*rec);
        }//Fin while
        file.close();
    }//Fin if
}//Fin loadRecords

void RecordManager::addRecord(Record rec){
	bool exist = false;

	for (std::map<int, Record>::iterator it = _records.begin(); 
            it != _records.end(); ++it){
		if(it->first == rec.getPlanet()){
			exist = true;
			if(it->second.getPoints() == rec.getPoints()){
				if(it->second.getTime() > rec.getTime()){
					_records[rec.getPlanet()] = rec;
				}//Fin if
			} else if(it->second.getPoints() > rec.getPoints()){
				_records[rec.getPlanet()] = rec;
			}//Fin if-else
			break;
		}//Fin if
	}//Fin for

	if(!exist){
		_records[rec.getPlanet()] = rec;
	}//Fin if
}//Fin addRecord

bool RecordManager::hasRecord(int planet){
    bool flag = false;

    for (std::map<int, Record>::iterator it = _records.begin(); 
            it != _records.end(); ++it){
        if(it->first == planet){
            flag = true;
            break;
        }//Fin if
    }//Fin for

    return flag;
}//Fin getRecord

Record RecordManager::getRecord(int planet){
	return _records[planet];
}//Fin getRecord

RecordManager* RecordManager::getSingletonPtr(){
    return msSingleton;
}//Fin getSingletonPtr

RecordManager& RecordManager::getSingleton(){  
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton