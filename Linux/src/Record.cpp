#include "Record.h"

Record::Record(): _time(99999), _points(0), _planet(-1){}
Record::Record(int planet, Ogre::Real time, int points): _time(time), 
	_points(points), _planet(planet){}

void Record::setPoints(int po){
	_points = po;
}//Fin setScore

void Record::setPlanet(int planet){
	_planet = planet;
}//Fin setPosition

void Record::setTime(Ogre::Real t){
	_time = t;
}//Fin setGamer

int Record::getPoints() const{
	return _points;
}//Fin getScore

int Record::getPlanet() const{
	return _planet;
}//Fin getPosition

Ogre::Real Record::getTime() const{
	return _time;
}//Fin getGamer

Ogre::String Record::toString(){
	int min, seg;
	std::stringstream ss("");

	ss << "P => " << _points << "\nT => ";
        
        min = (int) _time / 60; seg = (int) _time - (min * 60);

        if(min < 10){
            ss << "0";
        }//Fin if
        ss << min << ":";
        if(seg < 10){
            ss << "0";
        }//Fin if
        ss << seg;

	return ss.str();
}//Fin toString
