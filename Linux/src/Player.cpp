#include "Player.h"

Player::Player(std::string nombre, int tipo){
	_nombre = nombre;
	_tipo = tipo;
	_puntuacion = P_INIT;
	_lose = false;

   /*Cargamos los sonidos*/
   _pSoundFXManager = SoundFXManager::getSingletonPtr();
    _pTrackManager = TrackManager::getSingletonPtr();

    _choqueMetal = _pSoundFXManager->load("ChoqueMetal.ogg");
    _vida = _pSoundFXManager->load("Vida.ogg");
    _velocidad = _pSoundFXManager->load("Velocidad.ogg");
    _congelar = _pSoundFXManager->load("Congelar.ogg");

}//Fin constructor

Player::~Player(){
	if(_pj){
		delete _pj;
	}//Fin if
}//Fin destructor

int Player::getTipo(){
	return _tipo;
}//Fin getTipo

bool Player::lose(){
	return _lose;
}//Fin lose

std::string Player::getNombre(){
	return _nombre;
}//Fin getNombre

int Player::getPuntuacion(){
	return _puntuacion;
}//Fin getPuntuacion

Character* Player::getCharacter(){
	return _pj;
}//Fin getCharacter

void Player::setTipo(int tipo){
	_tipo = tipo;
}//Fin setTipo

void Player::setNombre(std::string nombre){
	_nombre = nombre;
}//Fin setNombre

void Player::setPuntuacion(int puntuacion){
	_puntuacion = puntuacion;
}//Fin setPuntuacion

void Player::lose(bool lose){
	_lose = lose;
}//Fin lose

void Player::setCharacter(Character* pj){
	_pj = pj;
}//Fin setCharacter

void Player::decreasePoints(){
	if(_puntuacion > 0){
		_puntuacion += -1;
	}//Fin if
}//Fin decreasePoints

int Player::getEstado(){
	return _estado;
}//Fin getEstado

void Player::setEstado(int estado){

	 std::string str2 ("Pix");

	//Para que los powerups solo suenen con el jugador 0
	if(estado!=_estado){

		if (_nombre.compare(str2) == 0){

				switch(estado){

				case 1:

					_velocidad->play(false);

					break;

				case 2:

					_congelar->play(false);

					break;

				case 3:

					_vida->play(false);

					break;

				default:

					_choqueMetal->play(false);

					break;

				}//Fin switch

		}//Fin if

		//si no es jugador uno solo choque normal

		else _choqueMetal->play(false);

	}//Fin if-else

	_estado=estado;

	//Se define el tiempo en funcion del estado
	switch (estado){
	//fuego
	case 1:
		_timeBooster = 5;
		break;
	//Hielo
	case 2:
		_timeBooster = 1.5;
		break;
	//Amarillo?
	case 3:
		//incrementa un punto
		_puntuacion += 1;
		break;
	default:
		_timeBooster = 5;
		break;

	}//Fin switch

}//Fin setEstado

Ogre::Real Player::getTime(){
	return _timeBooster;
}//Fin getTime

void Player::setTime(Ogre::Real deltaT){
	_timeBooster -= deltaT;
}//Fin setTime

