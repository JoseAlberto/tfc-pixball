#include "LevelSelectionState.h"
#include "RecordManager.h"
#include "PlayState.h"
#include "MenuState.h"

template<> LevelSelectionState* Ogre::Singleton<LevelSelectionState>::msSingleton = 0;

void LevelSelectionState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _selection = -1;
    _exitGame = _moveCamera = false;

    _planets.reserve(MAX_PLANETS);

    createScene();
    createOverlay();
}//Fin enter

void LevelSelectionState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    _ovLevels = _overlayManager->getByName("Levels");
    _ovMercury = _overlayManager->getByName("Mercury");
    _ovVenus = _overlayManager->getByName("Venus");
    _ovEarth = _overlayManager->getByName("Earth");
    _ovMars = _overlayManager->getByName("Mars");

    _o_name = _overlayManager->getOverlayElement("PlanetName");
    _o_score = _overlayManager->getOverlayElement("BestScore");
    _o_powerUps = _overlayManager->getOverlayElement("PowerUps");
    _o_dleft = _overlayManager->getOverlayElement("imgDirLeft");
    _o_dright = _overlayManager->getOverlayElement("imgDirRight");

    _o_dleft->hide();
    _o_powerUps->show();
    _o_score->show();
    _ovLevels->show();
    _ovMercury->show();
    _ovVenus->hide();
    _ovEarth->hide();
    _ovMars->hide();
}//Fin createOverlay

void LevelSelectionState::createScene(){
    Ogre::Entity *ent = NULL;
    Ogre::SceneNode *node = NULL;

    _camera->setPosition(Ogre::Vector3(-40, 0, 15));
    _camera->lookAt(Ogre::Vector3(-40, 0, 0));

    /* MERCURY */
    node = _sceneMgr->getRootSceneNode()->
        createChildSceneNode("Mercury");
    ent = _sceneMgr->createEntity("Mercury.mesh");
    node->attachObject(ent);
    node->setPosition(Ogre::Vector3(-40, 0, 0));
    node->scale(4, 4, 4);

    _planets.push_back(node);

    /* VENUS */
    node = _sceneMgr->getRootSceneNode()->
        createChildSceneNode("Venus");
    ent = _sceneMgr->createEntity("Venus.mesh");
    node->attachObject(ent);
    node->setPosition(Ogre::Vector3(-20, 0, 0));
    node->scale(4, 4, 4);

    _planets.push_back(node);

    /* EARTH */
    node = _sceneMgr->getRootSceneNode()->
        createChildSceneNode("Earth");
    ent = _sceneMgr->createEntity("Earth.mesh");
    node->attachObject(ent);
    node->setPosition(Ogre::Vector3(0, 0, 0));
    node->scale(4, 4, 4);

    _planets.push_back(node);

    /* MOON */
    _moon = node->createChildSceneNode("MoonNode");
    _moon->pitch(Ogre::Degree(-10));

    node = _moon->createChildSceneNode("Moon");
    ent = _sceneMgr->createEntity("Moon.mesh");
    ent->setCastShadows(false);
    node->attachObject(ent);
    node->setPosition(Ogre::Vector3(1, 0.40, 0));
    node->scale(0.5, 0.5, 0.5);

    /* MARS */
    node = _sceneMgr->getRootSceneNode()->
        createChildSceneNode("Mars");
    ent = _sceneMgr->createEntity("Mars.mesh");
    node->attachObject(ent);
    node->setPosition(Ogre::Vector3(20, 0, 0));
    node->scale(4, 4, 4);

    _planets.push_back(node);

    /* Background */
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("stars.jpg");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
     
    // Create background rectangle covering the whole screen
    _rect = new Ogre::Rectangle2D(true);
    _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    _rect->setMaterial("Background");
     
    // Render the background before everything else
    _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
     
    // Use infinite AAB to always stay visible
    Ogre::AxisAlignedBox aabInf;
    aabInf.setInfinite();
    _rect->setBoundingBox(aabInf);
     
    // Attach background to the scene
    node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Background");
    node->attachObject(_rect);

    /* Sombras */
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.75, 0.75, 0.75));

    /* Iluminacion */
    node = _sceneMgr->createSceneNode("LightingNode");
    Ogre::Light *light = _sceneMgr->createLight("Light");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(-0.5, -1, -1));
    node->attachObject(light);

    _selection = 0;
}//Fin createOverlay

void LevelSelectionState::exit() {
    delete _rect;

    _ovLevels->hide();
    _ovEarth->hide();
    _ovMercury->hide();
    _ovVenus->hide();
    _ovMars->hide();

    _planets.clear();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void LevelSelectionState::pause () {}

void LevelSelectionState::resume () {}

bool LevelSelectionState::frameStarted (const Ogre::FrameEvent& evt) {
    if(_moveCamera){
        float dist = _camera->getPosition().x - _planets[_selection]->getPosition().x;

        if(dist < -0.5 || dist > 0.5){
            _camera->move(_dir * evt.timeSinceLastFrame * 15);
            _camera->lookAt(Ogre::Vector3(_camera->getPosition().x, 0, 0));
        } else {
            _o_name->show();
            _moveCamera = false;
        }//Fin if-else
    }//Fin if

    switch(_selection){
        case MERCURY:
            if(!_moveCamera && !_o_dright->isVisible()){
                _o_dright->show();
            }//Fin if

            if(!_moveCamera && !_ovMercury->isVisible()){
                _ovMercury->show();
            }//Fin if

            _o_name->setCaption("MERCURY");
            break;
        case VENUS:
            if(!_moveCamera && !_o_dright->isVisible()){
                _o_dright->show();
            }//Fin if
            if(!_moveCamera && !_o_dleft->isVisible()){
                _o_dleft->show();
            }//Fin if

            if(!_moveCamera && !_ovVenus->isVisible()){
                _ovVenus->show();
            }//Fin if

            _o_name->setCaption("VENUS");
            break;
        case EARTH:
            if(!_moveCamera && !_o_dright->isVisible()){
                _o_dright->show();
            }//Fin if
            if(!_moveCamera && !_o_dleft->isVisible()){
                _o_dleft->show();
            }//Fin if

            if(!_moveCamera && !_ovEarth->isVisible()){
                _ovEarth->show();
            }//Fin if

            _o_name->setCaption("EARTH");
            _moon->yaw(Ogre::Degree(evt.timeSinceLastFrame * 30));
            break;
        case MARS:
            if(!_moveCamera && !_o_dleft->isVisible()){
                _o_dleft->show();
            }//Fin if

            if(!_moveCamera && !_ovMars->isVisible()){
                _ovMars->show();
            }//Fin if

            _o_name->setCaption("MARS");
            break;
    }//Fin switch

    if(_o_powerUps->isVisible() && _o_score->isVisible()){
        _o_powerUps->setCaption("Power-Ups");
        if(RecordManager::getSingletonPtr()->hasRecord(_selection)){
            _o_score->setCaption("Best Score\n\n\n" + RecordManager::getSingletonPtr(
                )->getRecord(_selection).toString());
        } else {
            _o_score->setCaption("Best Score\n\n\nP => --\nT => --:--");
        }//FIn if-else
    } else if(!_moveCamera){
        _o_powerUps->show();
        _o_score->show();
    }//Fin if-else

    if(_selection >= 0){
        _planets[_selection]->yaw(Ogre::Degree(evt.timeSinceLastFrame * 25));
    }//Fin if

    return true;
}//Fin frameStarted

bool LevelSelectionState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void LevelSelectionState::keyPressed (const OIS::KeyEvent &e) {
    switch(e.key){
        case OIS::KC_SPACE:
            if(!_moveCamera){
                PlayState::getSingletonPtr()->setLevel(_selection);
                changeState(PlayState::getSingletonPtr());
            }//Fin if
            break;
        case OIS::KC_LEFT:
            if(_selection > MERCURY){
                if(!_moveCamera){
                    _selection -= 1;
                    _dir = Ogre::Vector3(-1, 0, 0);

                    _ovEarth->hide();
                    _ovMercury->hide();
                    _ovVenus->hide();
                    _ovMars->hide();
                    _o_name->hide();
                    _o_dleft->hide();
                    _o_dright->hide();
                    _o_score->hide();
                    _o_powerUps->hide();

                    _moveCamera = true;
                }//Fin if
            }//Fin if
            break;
        case OIS::KC_RIGHT:
            if(_selection < MARS){
                if(!_moveCamera){
                    _selection += 1;
                    _dir = Ogre::Vector3(1, 0, 0);

                    _ovEarth->hide();
                    _ovMercury->hide();
                    _ovVenus->hide();
                    _ovMars->hide();
                    _o_name->hide();
                    _o_dleft->hide();
                    _o_dright->hide();
                    _o_score->hide();
                    _o_powerUps->hide();

                    _moveCamera = true;
                }//Fin if
            }//Fin if
            break;
        default:
            break;
    }//Fin switch
}//Fin keyPressed

void LevelSelectionState::keyReleased (const OIS::KeyEvent &e ) {
    if (e.key == OIS::KC_ESCAPE) {
        changeState(MenuState::getSingletonPtr());
    }//Fin if
}//Fin keyReleased

void LevelSelectionState::mouseMoved (const OIS::MouseEvent &e) {}

void LevelSelectionState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void LevelSelectionState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

LevelSelectionState* LevelSelectionState::getSingletonPtr () {
    return msSingleton;
}//Fin getSingletonPtr

LevelSelectionState& LevelSelectionState::getSingleton () { 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton