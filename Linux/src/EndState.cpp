#include "EndState.h"
#include "RecordManager.h"
#include "TrackManager.h"
#include "SoundFXManager.h"

template<> EndState* Ogre::Singleton<EndState>::msSingleton = 0;

void EndState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    _pSoundFXManager = SoundFXManager::getSingletonPtr();
     _pTrackManager = TrackManager::getSingletonPtr();

     _ganar = _pSoundFXManager->load("Ganar.ogg");
     _perder = _pSoundFXManager->load("Perder.ogg");

    _endGame = false;

    createOverlay();
}//Fin enter

void EndState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    _ovLose = _overlayManager->getByName("Lose");
    _ovExit = _overlayManager->getOverlayElement("LoseEText");

    _ovWin = _overlayManager->getByName("Win");
    _ovPoints = _overlayManager->getOverlayElement("PointsText");
    _ovExit = _overlayManager->getOverlayElement("WinEText");

    if(_mode == LOSE){
        _ovWin->hide();
        _ovLose->show();
        _perder->play(false);
    } else {
        _ovLose->hide();
        _ovWin->show();
        _ganar->play(false);
    }//Fin if
}//Fin createOverlay

void EndState::exit (){
    if(_ovLose->isVisible()){
        _ovLose->hide();
    } else {
        _ovWin->hide();
        Record rec (_selection, _time, _winner->getPuntuacion());
        RecordManager::getSingletonPtr()->addRecord(rec);
    }//Fin if-else
}//Fin exit

void EndState::result(int flag){
    _mode = flag;
}//Fin result

void EndState::result(int flag, Player* pj, int planet, Ogre::Real FinalT){
    _mode = flag;
    _winner = pj;
    _selection = planet;
    _time = FinalT;
}//Fin result

void EndState::pause (){}
void EndState::resume (){}

bool EndState::frameStarted(const Ogre::FrameEvent& evt){
    if(_ovLose->isVisible()){
        _ovExit->setCaption("Press Space to exit");
    } else if(_ovWin->isVisible()){
        int min, seg;
        std::stringstream ss;
        ss << "P => " << _winner->getPuntuacion() << "\nT => ";
        
        min = (int) _time / 60; seg = (int) _time - (min * 60);

        if(min < 10){
            ss << "0";
        }//Fin if
        ss << min << ":";
        if(seg < 10){
            ss << "0";
        }//Fin if
        ss << seg;

        _ovPoints->setCaption(ss.str());
        _ovExit->setCaption("Press Space to exit");
    }//Fin if-else

    return true;
}//Fin frameStarted

bool EndState::frameEnded(const Ogre::FrameEvent& evt){  
	if(_endGame){
        popState();
    }//Fin if
  
    return true;
}//Fin frameEnded

void EndState::keyPressed(const OIS::KeyEvent &e){
    switch(e.key){
        case OIS::KC_SPACE:
            /* Guardamos record */
            _endGame = true;
            break;
        default:
            break;
    }//Fin switch
}//Fin keyPressed

void EndState::keyReleased(const OIS::KeyEvent &e){}

EndState* EndState::getSingletonPtr (){
    return msSingleton;
}//Fin getSingletonPtr
EndState& EndState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void EndState::mouseMoved (const OIS::MouseEvent &e){}

void EndState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id){}

void EndState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id){}
