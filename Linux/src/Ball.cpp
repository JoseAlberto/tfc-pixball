#include "Ball.h"

Ball::Ball(bool active, OgreBulletDynamics::RigidBody * rigidBody,
		Ogre::SceneNode* sceneNode, int tipo) {
	_active = active;
	_rigidBall = rigidBody;
	_nodeBall = sceneNode;
	_tipo = tipo;
	_sonido=true;
}//Fin constructor

bool Ball::getActive(){
	return _active;
}//Fin getPuntuable

OgreBulletDynamics::RigidBody* Ball::getRigidBody(){
	return _rigidBall;
}//Fin getRigidBody

Ogre::SceneNode* Ball::getSceneNode(){
	return _nodeBall;
}//Fin getSceneNode

int Ball::getTipo(){
	return _tipo;
}//Fin getYipo

bool Ball::getSonar(){
	return _sonido;
}

void Ball::setActive(int active){
	_active = active;
}//Fin setPuntuable

void Ball::setRigidBody(OgreBulletDynamics::RigidBody* rigidBody){
	_rigidBall = rigidBody;
}//Fin setRigidBody

void Ball::setSceneNode(Ogre::SceneNode* sceneNode){
	_nodeBall = sceneNode;
}//Fin setSceneNode

void Ball::setTipo(int tipo){
	_tipo = tipo;
}//Fin setTipo

void Ball::setSonar(bool sonar){
	_sonido=sonar;
}
