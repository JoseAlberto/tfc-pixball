#include "IAManager.h"

template<> IAManager* Ogre::Singleton<IAManager>::msSingleton = 0;

#define ROTAR 0
#define GANAR 1
#define PERDER 2

IAManager::~IAManager(){
	reset();
}//Fin destructor

void IAManager::start(){
	_numActBalls = 0;
	_sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
}//Fin destructor

void IAManager::setPlayers(std::vector<Player*> js){
	_players.reserve(js.size());

	for(int i = 0;(unsigned) i < js.size(); i++){
		_players.push_back(js[i]);
	}//Fin for
}//Fin setPlayers

void IAManager::update(Ogre::Real deltaT){
    _deltaT = deltaT;

	if(_numActBalls > 0){
		updateIAS();
		updateVelocidad();
        updateBalls();
        updatePlayers();
	}//Fin if
}//Fin update

void IAManager::updateIAS(){
	int velocidad=1;
    /* Le asignamos un objetivo si de diera el caso */
    for(int i = 0; (unsigned)i < _balls.size(); i++){
        for(int j = 1; (unsigned)j < _players.size(); j++){
            if(_players[j]->getTipo() == T_IA && _players[j]->getPuntuacion() > 0){
                if(_players[j]->getCharacter()->thinkIn(_balls[i]->getRigidBody())){
                    _players[j]->getCharacter()->setObjetive(_balls[i]->getSceneNode());
                    break;
                }//Fin if
            }//Fin if
        }//Fin for
    }//Fin for

    /* Movemos las naves si fuera necesario */
    for(int i = 1; (unsigned)i < _players.size(); i++){
        if(_players[i]->getTipo() == T_IA && _players[i]->getPuntuacion() > 0) {

        	//Si el estado es fuego, doble de velocidad
        	if(_players[i]->getEstado()==1){
        		velocidad=2;
        	}

        	//Si el estado es hielo, estará detenido
        	if(_players[i]->getEstado()==2){
        		velocidad=0;
        	}

            if(_players[i]->getCharacter()->hasObjetive()){
                _players[i]->getCharacter()->moveToObjetive(_deltaT*velocidad);
            } else {
                _players[i]->getCharacter()->moveToCenter(_deltaT*velocidad);
            }//Fin if-else
        }//Fin if 
    }//Fin for
}//Fin updateIAS

void IAManager::reset(){
	_players.clear();

    Ball *aux;
    OgreBulletDynamics::RigidBody *rb;

	for(int i = 0; (unsigned) i < _balls.size(); i++){
        aux = _balls[i];
        rb = _balls[i]->getRigidBody();

        _balls.erase(_balls.begin() + i);
        delete aux; delete rb;
        i--;
    }//Fin for

	_balls.clear(); _numActBalls = 0;
}//Fin update

void IAManager::addFreeBall(Ball* ball){
	_balls.push_back(ball);
	_numActBalls++;
}//Fin registerBalls

void IAManager::updateBalls(){
    Ball *aux;
    Ogre::Vector3 pos;
	Ogre::SceneNode *nodo;
    OgreBulletDynamics::RigidBody *rb;

    for(int i = 0; (unsigned) i < _balls.size(); i++){
        pos = _balls[i]->getSceneNode()->getPosition();

        //Restaurar sonido
        if(pos.x<16 && pos.x>-16 && pos.z<16 && pos.z>-16)
        	_balls[i]->setSonar(true);

        if(pos.x < -25 && _balls[i]->getActive()){
            _balls[i]->setActive(false);
            _players[3]->decreasePoints();
            _players[3]->getCharacter()->setAnimation(PERDER);
            _players[0]->getCharacter()->setAnimation(GANAR);
        } else if(pos.x > 25 && _balls[i]->getActive()){
            _balls[i]->setActive(false);
            _players[2]->decreasePoints();
            _players[2]->getCharacter()->setAnimation(PERDER);
            _players[0]->getCharacter()->setAnimation(GANAR);
        } else if(pos.z < -25 && _balls[i]->getActive()){
            _balls[i]->setActive(false);
            _players[1]->decreasePoints();
            _players[1]->getCharacter()->setAnimation(PERDER);
            _players[0]->getCharacter()->setAnimation(GANAR);
        } else if(pos.z > 25 && _balls[i]->getActive()){
            _balls[i]->setActive(false);
            _players[0]->decreasePoints();
            _players[0]->getCharacter()->setAnimation(PERDER);
        } else if(pos.y < 400){
            aux = _balls[i];
            rb = _balls[i]->getRigidBody();
            nodo = _balls[i]->getSceneNode();

            _balls.erase(_balls.begin() + i);
            delete aux; delete rb;
            _sceneMgr->destroySceneNode(nodo->getName());
            i--; _numActBalls--;
        }//Fin if
    }//Fin for
}//Fin deleteSlept

int IAManager::getMode(){
	return _mode;
}//Fin getMode

int IAManager::getNumActBalls(){
	return _numActBalls;
}//Fin getNumActBalls

IAManager* IAManager::getSingletonPtr () {
    return msSingleton;
}//Fin getSingletonPtr

IAManager& IAManager::getSingleton () { 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void IAManager::updateVelocidad (){
	for(int i = 0; (unsigned) i < _balls.size(); i++){
		if(_balls[i]->getRigidBody()->getLinearVelocity().length() < 10){
		  _balls[i]->getRigidBody()->setLinearVelocity(
		  _balls[i]->getRigidBody()->getLinearVelocity() * 1.05);
        }//Fin if
	}//Fin for
}//Fin updateVelocidad

Ball* IAManager::getBooster(Ogre::SceneNode* node){
	Ogre::Vector3 aux(1,0,1);
	btTransform transform; //Declaration of the btTransform

	for(int i = 0; (unsigned) i < _balls.size(); i++){
			if(_balls[i]->getSceneNode() == node){

				if(_balls[i]->getTipo()==3){
					//Para que sea borrado
					_balls[i]->getSceneNode()->setPosition(0,398,0);
					Ogre::Vector3 position(_balls[i]->getSceneNode()->getPosition()); //Have to be initialized
					transform.setIdentity(); //This function put the variable of the object to default. The ctor of btTransform doesnt do it.
					transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(position)); //Set the new position/origin
					_balls[i]->getRigidBody()->getBulletRigidBody()->setWorldTransform(transform); //A
				}
				return _balls[i];
	        }//Fin if
		}//Fin for

	return 0;
}

void IAManager::updatePlayers(){
	//Decrementa el tiempo, si ha llegado a cero, reinicia el estado del jugador
	//Aprovechamos que se recorre el vector de jugadores para actualizar las
	//animaciones
	for(int i = 0; (unsigned)i < _players.size(); i++){

		_players[i]->getCharacter()->updateAnimation(_deltaT);

		if(_players[i]->getTime()>0 ){
			_players[i]->setTime(_deltaT);
		}
		else{
			_players[i]->setEstado(0);
		}//Fin if-else
	}//Fin for
}//Fin updatePlayers