#include "PlayState.h"
#include "Ball.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _pSoundFXManager = SoundFXManager::getSingletonPtr();
    _pTrackManager = TrackManager::getSingletonPtr();

    /* Background */
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

    /* Reservamos espacio para Playeres */
    _players.reserve(MAX_PLAYERS);
    /* Reservamos espacio para generadores */
    _generadores.reserve(MAX_GENERADORES);
    /* Reservamos espacio para los bloques de hielo */
    _iceBlocks.reserve(MAX_PLAYERS);
    /* Reservamos espacio para los bloques de fuego */
    _fireBlocks.reserve(MAX_PLAYERS);
    /* Reservamos espacio para los goals */
    _goals.reserve(MAX_GENERADORES);
    /* Reservamos espacio para los overlays */
    _bFaces.reserve(3); _rFaces.reserve(3);
    _yFaces.reserve(3); _gFaces.reserve(3);
    _points.reserve(MAX_PLAYERS);

    /* Inicializacion de variables */
    _time = 0.0;
    _timeLastShot = 0.0;

    _outPlayers = 0;
    _numBalls = _id = 0;
    _endGame = false;
    _exitGame = _lPress = _rPress = _firstperson = false;

    /*Cargamos los sonidos*/
    _choqueMetal = _pSoundFXManager->load("ChoqueMetal.ogg");

    /* Reinicio de semilla */
    srand(time(NULL));

    createScene();
    createOverlay();
    
    IAManager::getSingletonPtr()->start();
    IAManager::getSingletonPtr()->setPlayers(_players);   
}//Fin enter

void PlayState::createScene(){
	Ogre::Entity *ent = NULL;
	Ogre::SceneNode *node = NULL;
	Ogre::SceneNode *child = NULL;

	std::stringstream ss;

	_camera->setPosition(Ogre::Vector3(0, 640, 70));
	_camera->lookAt(Ogre::Vector3(0, 600, 0));

	/* Players */
	ss.str("Player_1");

	node = _sceneMgr->getRootSceneNode()->
			createChildSceneNode(ss.str());
	ent = _sceneMgr->createEntity("Nave.mesh");
	ent->setMaterialName("MaterialNave0");
	node->attachObject(ent);
	node->yaw(Ogre::Degree(180));
	node->setPosition(Ogre::Vector3(0, 604.25, 22.5));

    child = node->createChildSceneNode("ice1", Ogre::Vector3(0.5, -1, 0));
    ent = _sceneMgr->createEntity("Ice.mesh");
    child->attachObject(ent);
    child->setVisible(false);

    _iceBlocks.push_back(child);

    child = node->createChildSceneNode("fire1", Ogre::Vector3(-0.10, 0,0));
    ent = _sceneMgr->createEntity("Fire.mesh");
    child->attachObject(ent);
    child->setVisible(false);

    _fireBlocks.push_back(child);

	child = node->createChildSceneNode("Pix0",
			Ogre::Vector3(0, 1, 0));
	ent = _sceneMgr->createEntity("Pix.mesh");
	ent->setMaterialName("PixMaterial0");
	child->attachObject(ent);

	Player *jug = new Player("Pix", T_REAL);
	Character *pj = new Character(N_Z, Ogre::Vector3::NEGATIVE_UNIT_X, 
        Ogre::Vector3::UNIT_X, NULL, node, ent);
	jug->setCharacter(pj);
	_players.push_back(jug);

	ss.str("Player_2");

	node = _sceneMgr->getRootSceneNode()->
			createChildSceneNode(ss.str());
	ent = _sceneMgr->createEntity("Nave.mesh");
	ent->setMaterialName("MaterialNave1");
	node->attachObject(ent);
	node->setPosition(Ogre::Vector3(0, 604.25, -22.5));

    child = node->createChildSceneNode("ice2", Ogre::Vector3(0.5, -1, 0));
    ent = _sceneMgr->createEntity("Ice.mesh");
    child->attachObject(ent);
    child->setVisible(false);

    _iceBlocks.push_back(child);

    child = node->createChildSceneNode("fire2", Ogre::Vector3(-0.10, 0, 0));
    ent = _sceneMgr->createEntity("Fire.mesh");
    child->attachObject(ent);
    child->setVisible(false);

    _fireBlocks.push_back(child);

	child = node->createChildSceneNode("Pix1",
			Ogre::Vector3(0, 1, 0));
	ent = _sceneMgr->createEntity("Pix.mesh");
	ent->setMaterialName("PixMaterial1");
	child->attachObject(ent);

	jug = new Player(ss.str(), T_IA);
	pj = new Character(P_Z, Ogre::Vector3::NEGATIVE_UNIT_X, 
        Ogre::Vector3::UNIT_X, NULL, node, ent);
	jug->setCharacter(pj);
	_players.push_back(jug);

	ss.str("Player_3");

	node = _sceneMgr->getRootSceneNode()->
			createChildSceneNode(ss.str());
	ent = _sceneMgr->createEntity("Nave.mesh");
	ent->setMaterialName("MaterialNave2");
	node->attachObject(ent);
	node->setPosition(Ogre::Vector3(22.5, 604.25, 0));
	node->lookAt(Ogre::Vector3(0, 604, 0), Ogre::Node::TS_WORLD,
			Ogre::Vector3(0, 0, 1));

    child = node->createChildSceneNode("ice3", Ogre::Vector3(1, -1, 0));
    ent = _sceneMgr->createEntity("Ice.mesh");
    child->attachObject(ent);
    child->yaw(Ogre::Degree(90));
    child->setVisible(false);

    _iceBlocks.push_back(child);

    child = node->createChildSceneNode("fire3", Ogre::Vector3(-0.10, 0,0));
    ent = _sceneMgr->createEntity("Fire.mesh");
    child->attachObject(ent);
    child->setVisible(false);

    _fireBlocks.push_back(child);

	child = node->createChildSceneNode("Pix2",
			Ogre::Vector3(0, 1, 0));
	ent = _sceneMgr->createEntity("Pix.mesh");
	ent->setMaterialName("PixMaterial2");
	child->attachObject(ent);

	jug = new Player(ss.str(), T_IA);
	pj = new Character(N_X, Ogre::Vector3::UNIT_Z, 
        Ogre::Vector3::NEGATIVE_UNIT_Z, NULL, node, ent);
	jug->setCharacter(pj);
	_players.push_back(jug);

	ss.str("Player_4");

	node = _sceneMgr->getRootSceneNode()->
			createChildSceneNode(ss.str());
	ent = _sceneMgr->createEntity("Nave.mesh");
	ent->setMaterialName("MaterialNave3");

	node->attachObject(ent);
	node->setPosition(Ogre::Vector3(-22.5, 604.25, 0));
	node->lookAt(Ogre::Vector3(0, 604, 0), Ogre::Node::TS_WORLD,
			Ogre::Vector3(0, 0, 1));

    child = node->createChildSceneNode("ice4", Ogre::Vector3(-1, -1, 0));
    ent = _sceneMgr->createEntity("Ice.mesh");
    child->attachObject(ent);
    child->yaw(Ogre::Degree(180));
    child->setVisible(false);

    _iceBlocks.push_back(child);

    child = node->createChildSceneNode("fire4", Ogre::Vector3(-0.10, 0,0));
    ent = _sceneMgr->createEntity("Fire.mesh");
    child->attachObject(ent);
    child->setVisible(false);

    _fireBlocks.push_back(child);

	child = node->createChildSceneNode("Pix3",
			Ogre::Vector3(0, 1, 0));
	ent = _sceneMgr->createEntity("Pix.mesh");
	ent->setMaterialName("PixMaterial3");
	child->attachObject(ent);

	jug = new Player(ss.str(), T_IA);
	pj = new Character(P_X, Ogre::Vector3::UNIT_Z, 
        Ogre::Vector3::NEGATIVE_UNIT_Z, NULL, node, ent);
	jug->setCharacter(pj);

	_players.push_back(jug);

	/* Generadores */
	for(int i = 0; i < MAX_GENERADORES; i++){
		ss.str("Generador_");
		ss << (i);
		node = _sceneMgr->getRootSceneNode()->
				createChildSceneNode(ss.str());
		ent = _sceneMgr->createEntity("Generador.mesh");
		node->attachObject(ent);

		_generadores.push_back(node);
	}//Fin for

	/* Localizacion de los generadores en el escenario */
	_generadores[0]->setPosition(Ogre::Vector3(18.5, 604.25, 18.5));
	_generadores[0]->lookAt(_players[3]->getCharacter()->getNave()->getPosition(),
			Ogre::Node::TS_WORLD);
	_generadores[1]->setPosition(Ogre::Vector3(18.5, 604.25, -18.5));
	_generadores[1]->lookAt(_players[0]->getCharacter()->getNave()->getPosition(),
			Ogre::Node::TS_WORLD);
	_generadores[2]->setPosition(Ogre::Vector3(-18.5, 604.25, 18.5));
	_generadores[2]->lookAt(_players[1]->getCharacter()->getNave()->getPosition(),
			Ogre::Node::TS_WORLD);
	_generadores[3]->setPosition(Ogre::Vector3(-18.5, 604.25, -18.5));
	_generadores[3]->lookAt(_players[2]->getCharacter()->getNave()->getPosition(),
			Ogre::Node::TS_WORLD);

	/* Creamos las fisicas del juego */
	createWorld();

    // Attach background to the scene
    node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Background");
    node->attachObject(_rect);

	/* Sombras */
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	_sceneMgr->setAmbientLight(Ogre::ColourValue(0.75, 0.75, 0.75));

	/* Iluminacion */
	node = _sceneMgr->createSceneNode("LightingNode");
	Ogre::Light *light = _sceneMgr->createLight("Light");
	light->setType(Ogre::Light::LT_DIRECTIONAL);
	light->setDirection(Ogre::Vector3(-0.5, -1, -1));
	node->attachObject(light);
}//Fin createScene

void PlayState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();

    _ovPlay = _overlayManager->getByName("PlayPoints");
    _ovBlue = _overlayManager->getByName("Blue");
    _ovRed = _overlayManager->getByName("Red");
    _ovYellow = _overlayManager->getByName("Yellow");
    _ovGreen = _overlayManager->getByName("Green");

    _bFaces.push_back(_overlayManager->getOverlayElement("B_Normal"));
    _bFaces.push_back(_overlayManager->getOverlayElement("B_Ungry"));
    _bFaces.push_back(_overlayManager->getOverlayElement("B_Laugh"));

    _bFaces[0]->hide();
    _bFaces[1]->hide();

    _rFaces.push_back(_overlayManager->getOverlayElement("R_Normal"));
    _rFaces.push_back(_overlayManager->getOverlayElement("R_Ungry"));
    _rFaces.push_back(_overlayManager->getOverlayElement("R_Laugh"));

    _rFaces[0]->hide();
    _rFaces[1]->hide();

    _yFaces.push_back(_overlayManager->getOverlayElement("Y_Normal"));
    _yFaces.push_back(_overlayManager->getOverlayElement("Y_Ungry"));
    _yFaces.push_back(_overlayManager->getOverlayElement("Y_Laugh"));

    _yFaces[0]->hide();
    _yFaces[1]->hide();

    _gFaces.push_back(_overlayManager->getOverlayElement("G_Normal"));
    _gFaces.push_back(_overlayManager->getOverlayElement("G_Ungry"));
    _gFaces.push_back(_overlayManager->getOverlayElement("G_Laugh"));

    _gFaces[0]->hide();
    _gFaces[1]->hide();

    _points.push_back(_overlayManager->getOverlayElement("BluePoints"));
    _points.push_back(_overlayManager->getOverlayElement("RedPoints"));
    _points.push_back(_overlayManager->getOverlayElement("YellowPoints"));
    _points.push_back(_overlayManager->getOverlayElement("GreenPoints"));

    _ovTime = _overlayManager->getOverlayElement("TimeOv");

    _ovBlue->show();
    _ovRed->show();
    _ovYellow->show();
    _ovGreen->show();
    _ovPlay->show();
}//Fin createScene

void PlayState::createWorld(){
    Ogre::SceneNode* node = NULL;
    Ogre::Entity* ent = NULL;

    OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL; 
    OgreBulletCollisions::CollisionShape *bodyShape = NULL;
    OgreBulletDynamics::RigidBody *rigidBody = NULL;

    Ogre::Vector3 pos;
    Ogre::Quaternion ori;
    std::stringstream ss;

    /************************* Mundo *************************/
    _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    _debugDrawer->setDrawWireframe(true);  
    node = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "debugNode", Ogre::Vector3::ZERO);
    node->attachObject(static_cast<Ogre::SimpleRenderable *>(_debugDrawer));

    Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
        Ogre::Vector3 (-10000, -10000, -10000), 
        Ogre::Vector3 (10000,  10000,  10000));
    Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

    _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
        worldBounds, gravity);
    _world->setDebugDrawer (_debugDrawer);
    _world->setShowDebugShapes (false);  // Muestra los collision shapes

    /************************* Plataforma *************************/
    OgreBulletCollisions::CylinderCollisionShape *cylbody =
             new OgreBulletCollisions::CylinderCollisionShape
             (Ogre::Vector3(33, 2.5, 33), Ogre::Vector3::UNIT_Y);

    ent = _sceneMgr->createEntity("Plataforma.mesh");
    node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Plataforma",
        Ogre::Vector3(0, 600, 0));
    node->attachObject(ent);

    _rbStage = new OgreBulletDynamics::RigidBody("rigidBodyPlataform", _world);

    _rbStage->setStaticShape(cylbody, 0.6 /* Restitucion */, 
        0.8 /* Friccion */, Ogre::Vector3(0, 600, 0));

    _shapes.push_back(cylbody);

    /************************* Generadores *************************/

    cylbody = new OgreBulletCollisions::CylinderCollisionShape(Ogre::Vector3(4.5, 1, 4.5), 
        Ogre::Vector3::UNIT_Y);

    for(int i = 0;(unsigned) i < _generadores.size(); i++){
        ss.str("rigidBodyGen_"); 
        ss << i;

        rigidBody = new OgreBulletDynamics::RigidBody(ss.str(), _world);

        rigidBody->setStaticShape(cylbody, 0.6 /* Restitucion */, 
            0.8 /* Friccion */, _generadores[i]->getPosition());

        _genBodies.push_back(rigidBody);
    }//Fin for

    _shapes.push_back(cylbody);

    /************************* Naves *************************/

    for(int i = 0; (unsigned) i < _players.size(); i++){
        ss.str("rigidBodyNav_"); 
        ss << i;

        ent = static_cast<Ogre::Entity*>(_players[i]->getCharacter()->getNave(
            )->getAttachedObject(0));

        trimeshConverter = new 
        OgreBulletCollisions::StaticMeshToShapeConverter(ent);
        bodyShape = trimeshConverter->createConvex();
        delete trimeshConverter;

        rigidBody = new OgreBulletDynamics::RigidBody(ss.str(), _world);

        pos = _players[i]->getCharacter()->getNave()->getPosition();
        ori = _players[i]->getCharacter()->getNave()->getOrientation();

        rigidBody->setShape(_players[i]->getCharacter()->getNave(),
            bodyShape, 0.6 /* Restitucion */, 0.8 /* Friccion */, 5.0 /* Masa */,
           pos, ori);

        rigidBody->setKinematicObject(true);

        _players[i]->getCharacter()->setRigidBody(rigidBody);

        _shapes.push_back(bodyShape);   _navBodies.push_back(rigidBody);
    }//Fin for
}//Fin createWorld

void PlayState::setLevel(int level){
    std::stringstream sm, st;

    _level = level;

    switch(level){
        /* Mercury */
        case 0:
            sm.str("mercury_back");
            st.str("mercury_back.jpg");
            break;
        /* Venus */
        case 1:
            sm.str("venus_back");
            st.str("venus_back.jpg");
            break;
        /* Earth */
        case 2:
            sm.str("earth_back");
            st.str("earth_back.jpg");
            break;
        /* Mars */
        case 3:
            sm.str("mars_back");
            st.str("mars_back.jpg");
            break;
    }//Fin switch

    /* Background */
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create(sm.str(), "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState(st.str());
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
     
    // Create background rectangle covering the whole screen
    _rect = new Ogre::Rectangle2D(true);
    _rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    _rect->setMaterial(sm.str());
     
    // Render the background before everything else
    _rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
     
    // Use infinite AAB to always stay visible
    Ogre::AxisAlignedBox aabInf;
    aabInf.setInfinite();
    _rect->setBoundingBox(aabInf);
}//Fin setLevel

void PlayState::updatePlayer(){
	int velocidad = 1;
	switch(_id){
    	case 0:
    		/*Player 1*/
    		if(_players[0]->getEstado() == 1){
    			velocidad = 2;
    		}//Fin if

    		if(_players[0]->getEstado() == 2){
    			velocidad = 0;
    		}//Fin if

    		if(_rPress) {
    			if( _players[0]->getCharacter()->getNave()->getPosition().x < 11.33)
    				_players[0]->getCharacter()->move(RIGHT, _deltaT*velocidad);
    		} else if(_lPress) {
    			if( _players[0]->getCharacter()->getNave()->getPosition().x > -11.31)
    				_players[0]->getCharacter()->move(LEFT, _deltaT*velocidad);
    		}//Fin if-else
    		break;
    	case 1:
    		/*Player 2*/
    		if(_rPress) {
    			if( _players[1]->getCharacter()->getNave()->getPosition().x > -11.31)
    				_players[1]->getCharacter()->move(LEFT, _deltaT);
    		} else if(_lPress) {
    			if( _players[1]->getCharacter()->getNave()->getPosition().x < 11.33)
    				_players[1]->getCharacter()->move(RIGHT, _deltaT);
    		}//Fin if-else
    		break;
    	case 2:
    		/*Player 3*/
    		if(_rPress) {
    			if( _players[2]->getCharacter()->getNave()->getPosition().z > -11.10)
    				_players[2]->getCharacter()->move(RIGHT, _deltaT);
    		} else if(_lPress) {
    			if( _players[2]->getCharacter()->getNave()->getPosition().z < 11.10)
    				_players[2]->getCharacter()->move(LEFT, _deltaT);
    		}//Fin if-else
    		break;
    	case 3:
    		/*Player 4*/
    		if(_rPress) {
    			if(_players[3]->getCharacter()->getNave()->getPosition().z < 11.10)
    				_players[3]->getCharacter()->move(LEFT, _deltaT);
    		} else if(_lPress) {
    			if(_players[3]->getCharacter()->getNave()->getPosition().z > -11.10)
    				_players[3]->getCharacter()->move(RIGHT, _deltaT);
    		}//Fin if-else
            break;
	}//Fin switch
}//Fin updatePlayer

void PlayState::shootBall(){
    _timeLastShot += _deltaT;

    int booster = 0;

    if(_timeLastShot >= MIN_TIME && IAManager::getSingletonPtr()->getNumActBalls() < MAX_BALLS){
    	unsigned int generador;

        Ogre::Entity *ent = _sceneMgr->createEntity("asteroid_" +
                Ogre::StringConverter::toString(_numBalls), "Asteroid.mesh");

        Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
            createChildSceneNode();
        node->attachObject(ent);

        OgreBulletCollisions::SphereCollisionShape *bodyShape = 
            new OgreBulletCollisions::SphereCollisionShape(1);
        OgreBulletDynamics::RigidBody *rigidBody = new OgreBulletDynamics::RigidBody(
            "rigidBody" + Ogre::StringConverter::toString(_numBalls), _world);

        Ogre::Vector3 posRigid;
        Ogre::Vector3 pos;

    	generador = (unsigned)rand() % 4;

        switch(generador){
        case 0:
        	posRigid = _generadores[0]->getPosition();
        	pos = _generadores[0]->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
        	break;
        case 1:
        	posRigid = _generadores[1]->getPosition();
        	pos = _generadores[1]->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
        	break;
        case 2:
        	posRigid = _generadores[2]->getPosition();
        	pos = _generadores[2]->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
        	break;
        case 3:
        	posRigid = _generadores[3]->getPosition();
        	pos = _generadores[3]->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
        	break;
        default:
        	posRigid = _generadores[3]->getPosition();
        	pos = _generadores[3]->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
        	break;
        }//Fin switch

        rigidBody->setShape(node, bodyShape,
            			0.6 /* Restitucion */, 0 /* Friccion */,
            			0.001/* Masa */,
            			Ogre::Vector3(posRigid.x, posRigid.y + 3, posRigid.z),
            			Ogre::Quaternion::IDENTITY /* Orientacion */);
        rigidBody->getBulletRigidBody()->setAngularFactor(btVector3(0, 0, 0));

        rigidBody->setLinearVelocity(pos * 10);

        //En mercurio(0) y Venus(1) solo hay un booster
        if(_level== 0 || _level==1){
        	booster = (unsigned)rand() % 2;

        	if(_level==1 && booster==1)
        		booster=2;
        }

        //En tierra y marte hay 3
        else if (_level==2){

        	do{
        	booster = (unsigned)rand() % 4;
        	}while(booster==1);//Evita que sea fuego

        }
        else {
        	do{
        		booster = (unsigned)rand() % 4;
        	}while(booster==2);//Evita que sea hielo
        }//Fin if-else

        ent->setMaterialName("Asteroid_Mat" +
                Ogre::StringConverter::toString(booster) );

        Ball* ball = new Ball(true, rigidBody, node, booster);

        _shapes.push_back(bodyShape);
        IAManager::getSingletonPtr()->addFreeBall(ball);

        _numBalls++;

        _timeLastShot = 0.0;
    }//Fin if
}//Fin shootBall

void PlayState::exit () {

    /* Eliminar cuerpos rigidos Naves */
    std::deque <OgreBulletDynamics::RigidBody *>::iterator 
        itBody = _navBodies.begin();
    while (_navBodies.end() != itBody) {   
        delete *itBody;  ++itBody;
    }//Fin while

    /* Eliminar cuerpos rigidos Generadores */
    itBody = _genBodies.begin();
    while (_genBodies.end() != itBody) {   
        delete *itBody;  ++itBody;
    }//Fin while

    /* Eliminar cuerpos rigidos Barreras */
    itBody = _wallBodies.begin();
    while (_wallBodies.end() != itBody) {   
        delete *itBody;  ++itBody;
    }//Fin while

    /* Eliminar plataforma */
    delete _rbStage;

    /* Eliminar formas de colision */
    std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
        itShape = _shapes.begin();
    while (_shapes.end() != itShape) {   
        delete *itShape; ++itShape;
    }//Fin while

    std::vector<Player*>::iterator itJug = _players.begin();
    while(_players.end() != itJug){
        delete *itJug; ++itJug;
    }//Fin while

    _iceBlocks.clear(); _fireBlocks.clear();
    _players.clear(); _generadores.clear();
    _navBodies.clear(); _genBodies.clear();
    _wallBodies.clear(); _shapes.clear();

    /* Reiniciamos el gestior de la IA */
    IAManager::getSingletonPtr()->reset();

    /* Eliminar mundo dinamico y debugDrawer */
    delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
    delete _world; 

    /* Eliminar background */
    delete _rect;

    _ovBlue->hide();
    _ovRed->hide();
    _ovYellow->hide();
    _ovGreen->hide();
    _ovPlay->hide();

    _bFaces.clear(); _rFaces.clear();
    _yFaces.clear(); _gFaces.clear();
    _points.clear();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void PlayState::pause() {
    _ovBlue->hide();
    _ovRed->hide();
    _ovYellow->hide();
    _ovGreen->hide();
    _ovPlay->hide();
}//Fin pause

void PlayState::resume() {
    if(_endGame){
        changeState(MenuState::getSingletonPtr());
    } else {
        _ovBlue->show();
        _ovRed->show();
        _ovYellow->show();
        _ovGreen->show();
        _ovPlay->show();
    }//Fin if-else
}//Fin resume

void PlayState::updateOverlays(){
    int seg, min;
    std::stringstream sc;

    for(int i = 0; (unsigned) i < _players.size(); i++){
        if(_players[i]->getPuntuacion() >= 10){
            switch(i){
                case 0:
                    if(!_bFaces[2]->isVisible()){
                        _bFaces[1]->hide();
                        _bFaces[2]->show();
                    }//Fin if
                    break;
                case 1:
                    if(!_rFaces[2]->isVisible()){
                        _rFaces[1]->hide();
                        _rFaces[2]->show();
                    }//Fin if
                    break;
                case 2:
                    if(!_yFaces[2]->isVisible()){
                        _yFaces[1]->hide();
                        _yFaces[2]->show();
                    }//Fin if
                    break;
                case 3:
                    if(!_gFaces[2]->isVisible()){
                        _gFaces[1]->hide();
                        _gFaces[2]->show();
                    }//Fin if
                    break;
            }//Fin switch
        } else if(_players[i]->getPuntuacion() < 10 && 
            _players[i]->getPuntuacion() >= 5){
            sc << "0";
            switch(i){
                case 0:
                    if(!_bFaces[0]->isVisible()){
                        _bFaces[1]->hide();
                        _bFaces[2]->hide();
                        _bFaces[0]->show();
                    }//Fin if
                    break;
                case 1:
                    if(!_rFaces[0]->isVisible()){
                        _rFaces[1]->hide();
                        _rFaces[2]->hide();
                        _rFaces[0]->show();
                    }//Fin if
                    break;
                case 2:
                    if(!_yFaces[0]->isVisible()){
                        _yFaces[1]->hide();
                        _yFaces[2]->hide();
                        _yFaces[0]->show();
                    }//Fin if
                    break;
                case 3:
                    if(!_gFaces[0]->isVisible()){
                        _gFaces[1]->hide();
                        _gFaces[2]->hide();
                        _gFaces[0]->show();
                    }//Fin if
                    break;
            }//Fin switch
        } else {
            sc << "0";
            switch(i){
                case 0:
                    if(!_bFaces[1]->isVisible()){
                        _bFaces[0]->hide();
                        _bFaces[1]->show();
                    }//Fin if
                    break;
                case 1:
                    if(!_rFaces[1]->isVisible()){
                        _rFaces[0]->hide();
                        _rFaces[1]->show();
                    }//Fin if
                    break;
                case 2:
                    if(!_yFaces[1]->isVisible()){
                        _yFaces[0]->hide();
                        _yFaces[1]->show();
                    }//Fin if
                    break;
                case 3:
                    if(!_gFaces[1]->isVisible()){
                        _gFaces[0]->hide();
                        _gFaces[1]->show();
                    }//Fin if
                    break;
            }//Fin switch
        }//Fin if-else

        sc << _players[i]->getPuntuacion();
        _points[i]->setCaption(sc.str()); sc.str("");
    }//Fin for

    min = (int) _time / 60; seg = (int) _time - (min * 60);

    if(min < 10){
        sc << "0";
    }//Fin if
    sc << min << ":";
    if(seg < 10){
        sc << "0";
    }//Fin if
    sc << seg;

    _ovTime->setCaption(sc.str());
}//Fin updateOverlays

void PlayState::activateWall(int idx){
    Ogre::SceneNode* wall = NULL;
    Ogre::Entity* ent = NULL;

    OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = NULL; 
    OgreBulletCollisions::CollisionShape *bodyShape = NULL;
    OgreBulletDynamics::RigidBody *rigidBody = NULL;

    wall = _sceneMgr->getRootSceneNode()->createChildSceneNode(
        "wallnode" + Ogre::StringConverter::toString(_outPlayers));
    ent = _sceneMgr->createEntity("wall_" +
            Ogre::StringConverter::toString(_outPlayers), "Wall.mesh");

    switch(idx){
        case 1:
            _players[idx]->getCharacter()->getNave()->setVisible(false);
            _players[idx]->getCharacter()->getNave()->setPosition(
                Ogre::Vector3(-20, 0, 0));
            wall->setPosition(Ogre::Vector3(0, 603, -22));
            wall->yaw(Ogre::Degree(90));
            break;
        case 2:
            _players[idx]->getCharacter()->getNave()->setVisible(false);
            _players[idx]->getCharacter()->getNave()->setPosition(
                Ogre::Vector3(0, 0, 0));
            wall->setPosition(Ogre::Vector3(22, 603, 0));
            break;
        case 3:
            _players[idx]->getCharacter()->getNave()->setVisible(false);
            _players[idx]->getCharacter()->getNave()->setPosition(
                Ogre::Vector3(20, 0, 0));
            wall->setPosition(Ogre::Vector3(-22, 603, 0));
            break;
    }//Fin switch
    wall->attachObject(ent);

    trimeshConverter = new 
        OgreBulletCollisions::StaticMeshToShapeConverter(ent);
    bodyShape = trimeshConverter->createConvex();
    delete trimeshConverter;

    rigidBody = new OgreBulletDynamics::RigidBody("wallbody" 
        + Ogre::StringConverter::toString(_outPlayers), _world);

    Ogre::Vector3 pos = wall->getPosition();
    Ogre::Quaternion ori = wall->getOrientation();

    rigidBody->setShape(wall, bodyShape, 0.6 /* Restitucion */, 
        0.8 /* Friccion */, 5.0 /* Masa */, pos, ori);

    rigidBody->setKinematicObject(true);

    _shapes.push_back(bodyShape);   _wallBodies.push_back(rigidBody);
}//Fin activateWall

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {

    if(!_endGame){
        _deltaT = evt.timeSinceLastFrame;
        _time += _deltaT;

        _world->stepSimulation(_deltaT);

        /* Movimiento Player */
        updatePlayer();
        /* Lanzamiento Balls nuevas */
        shootBall();
        /* Actualización IA */
        IAManager::getSingletonPtr()->update(_deltaT);
        /* Actualización Overlays */
        updateOverlays();
        /*Deteccion de colisiones*/
        DetectCollisionDrain();
        /*Congelar jugadores*/
        updatePlayerEffects();

        if(_firstperson){
    		_camera->setPosition(_players[0]->getCharacter()->getNave()->getPosition().x,
           		_players[0]->getCharacter()->getNave()->getPosition().y + 8,
           		_players[0]->getCharacter()->getNave()->getPosition().z + 9);
        	//_camera->lookAt(Ogre::Vector3(0, 600, 0));
        }//Fin if

        /* Comprobamos puntaciones */
        if(_players[0]->getPuntuacion() == 0){
            _endGame = true;
            EndState::getSingletonPtr()->result(LOSE);
            pushState(EndState::getSingletonPtr());
        } else {
            for(int i = 1; (unsigned)i < _players.size(); i++){
                if(!_players[i]->lose()){
                    if(_players[i]->getPuntuacion() == 0){
                        _outPlayers++;
                        _players[i]->lose(true);
                        activateWall(i);
                    }//Fin if
                }//Fin if
            }//Fin for

            if(_outPlayers == 3){
                _endGame = true;
                EndState::getSingletonPtr()->result(WIN, _players[0], 
                    _level, _time);
                pushState(EndState::getSingletonPtr());
            }//Fin if
        }//Fin if-else
    }//Fin if

    return true;
}//Fin frameStarted

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
    _deltaT = evt.timeSinceLastFrame;

    _world->stepSimulation(_deltaT);

    if (_exitGame){
        return false;
    }//Fin if
  
    return true;
}//Fin frameEnded

void PlayState::keyPressed (const OIS::KeyEvent &e) {
	switch(e.key){
	case OIS::KC_RIGHT:
		_rPress = true;
		break;
	case OIS::KC_LEFT:
		_lPress = true;
		break;
	case OIS::KC_I:
		if(_firstperson){
			_camera->setPosition(Ogre::Vector3(0, 640, 70));
			_camera->lookAt(Ogre::Vector3(0, 600, 0));
			_firstperson = false;
		} else {
			_firstperson = true;
			_camera->lookAt(Ogre::Vector3(0, 608, 0));
		}//Fin if-else
			break;

	case OIS::KC_J:
		_players[1]->setPuntuacion(0);
		break;
	case OIS::KC_K:
		_players[2]->setPuntuacion(0);
		break;
	case OIS::KC_L:
		_players[3]->setPuntuacion(0);
		break;

    default:
        break;
    }//Fin switch
}//Fin keyPressed

void PlayState::keyReleased (const OIS::KeyEvent &e) {
	switch(e.key){
	case OIS::KC_ESCAPE:
		pushState(PauseState::getSingletonPtr());
		break;
	case OIS::KC_RIGHT:
		_rPress = false;
		break;
	case OIS::KC_LEFT:
		_lPress = false;
		break;
	default:
		break;
	}//Fin switch
}//Fin keyReleased

void PlayState::mouseMoved (const OIS::MouseEvent &e) {}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

PlayState* PlayState::getSingletonPtr () {
    return msSingleton;
}//Fin getSingletonPtr

PlayState& PlayState::getSingleton () { 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void PlayState::endGame(){
    _endGame = true;
}//Fin endGame


void PlayState::DetectCollisionDrain() {

	Ball *bolaGolpeada=NULL;

    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i = 0; i < numManifolds;i++) {
        btPersistentManifold* contactManifold =
          bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        btCollisionObject* obA =
          const_cast<btCollisionObject*>(contactManifold->getBody0());
        btCollisionObject* obB =
          const_cast<btCollisionObject*>(contactManifold->getBody1());

        Ogre::SceneNode* ground = _sceneMgr->getSceneNode("Plataforma");

        OgreBulletCollisions::Object *obGround = _world->findObject(ground);

        OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
        OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);

        Ogre::SceneNode* node = NULL;
        Ogre::SceneNode* goal = NULL;

        for(int i = 0; (unsigned) i < _players.size(); i++){
        	goal = _sceneMgr->getSceneNode("Player_" + Ogre::StringConverter::toString(i+1));
        	OgreBulletCollisions::Object *obDrain = _world->findObject(goal);

        	/* Se evitan las colisiones con el suelo */
        	if((obOB_A != obGround) && (obOB_B != obGround)){

        		if ((obOB_A == obDrain) || (obOB_B == obDrain)) {

        			if ((obOB_A != obDrain) && (obOB_A)) {
        				node = obOB_A->getRootNode();
        			} else if ((obOB_B != obDrain) && (obOB_B)) {
        				node = obOB_B->getRootNode();
        			}//Fin if-else

        			if (node) {
        				//Conocer el booster de la bola que ha chocado
        				bolaGolpeada = IAManager::getSingletonPtr()->getBooster(node);
                        if(bolaGolpeada){
            				if(bolaGolpeada->getTipo()){
            					_players[i]->setEstado(bolaGolpeada->getTipo());
            				}//fin if booster
            				else{
            					if(bolaGolpeada->getSonar()){
            						_choqueMetal->play(false);
            						bolaGolpeada->setSonar(false);
            					}//Fin if
            				}//Fin if-else
                        }//Fin if
        			}//Fin if
        		}//Fin if
        	}//Fin si es suelo
        }//Fin for
    }//Fin for numManifolds
}//Fin DetectCollisionDrain

void PlayState::updatePlayerEffects(){
	for(int i=0; (unsigned) i < _iceBlocks.size(); i++){
		if(_players[i]->getEstado()==2){
			_iceBlocks[i]->setVisible(true);
		}
		else if(_players[i]->getEstado()==1){
			_fireBlocks[i]->setVisible(true);
		}
		else{
			_iceBlocks[i]->setVisible(false);
			_fireBlocks[i]->setVisible(false);
		}//Fin if-else
	}//Fin for
}//Fin updatePlayerEffects
