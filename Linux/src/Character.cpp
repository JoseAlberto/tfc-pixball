#include "Character.h"
#include <iostream>

#define ROTAR 0
#define GANAR 1
#define PERDER 2


Character::Character(int type, Ogre::Vector3 left, Ogre::Vector3 right, Ogre::SceneNode* pix, 
	Ogre::SceneNode* nave, Ogre::Entity *ent){
	_type = type;
	_mleft = left;
	_mright = right;
	_pix = pix;
	_nave = nave;
	_objetive = 0;

	_animation=ROTAR;

	/*Rotar*/
	_animRotar = ent->getAnimationState("ReposoRotar");
	_animRotar->setEnabled(true);
	_animRotar->setTimePosition(0);
	_animRotar->setLoop(true);

	/*Ganar*/
	_animGanar = ent->getAnimationState("Ganar");
	_animGanar->setEnabled(false);
	_animGanar->setTimePosition(0);
	_animGanar->setLoop(false);

	/*Perder*/
	_animPerder = ent->getAnimationState("Perder");
	_animPerder->setEnabled(false);
	_animPerder->setTimePosition(0);
	_animPerder->setLoop(false);

	_center = nave->getPosition();
}//Fin constructor

void Character::setPix(Ogre::SceneNode* nodo){
	_pix = nodo;
}//Fin setPix

void Character::setNave(Ogre::SceneNode* nodo){
	_nave = nodo;
}//Fin setNave

void Character::setRigidBody(OgreBulletDynamics::RigidBody* rb){
	_rbNave = rb;
}//Fin setRigidBody

void Character::setObjetive(Ogre::SceneNode* ball){
	_objLastPos = ball->getPosition();

	_objetive = ball;
}//Fin setObjetive

void Character::move(int flag, Ogre::Real deltaT){
	Ogre::Vector3 dir = Ogre::Vector3::ZERO;

	switch(flag){
		case RIGHT:
			dir = _mright;
			break;
		case LEFT:
			dir = _mleft;
			break;
	}//Fin switch

	_rbNave->enableActiveState();
	_nave->translate(dir * deltaT * NAV_VEL);
}//Fin setImpulso

bool Character::hasObjetive(){
	bool flag = false;

	if(_objetive){
		flag = true;
	}//Fin if

	return flag;
}//Fin hasNext

void Character::moveToCenter(Ogre::Real deltaT){
	if(_nave->getPosition() != _center){
		Ogre::Vector3 vtrans = _center - _nave->getPosition();


		if(abs(vtrans.x + vtrans.y + vtrans.z) > 1){
			_nave->translate(vtrans.normalisedCopy() * deltaT * NAV_VEL); 
		}//Fin if
	}//Fin if
}//Fin moveToCenter

void Character::moveToObjetive(Ogre::Real deltaT){
	float dif = 0;
	Ogre::Vector3 dir = Ogre::Vector3::ZERO;

	if(_objetive->getPosition().x > -22.5 && _objetive->getPosition().x < 22.5 &&
		_objetive->getPosition().z > -22.5 && _objetive->getPosition().z < 22.5){
		switch(_type){
			case P_X:
				/* Si el objetivo se acerca nos movemos */
				if(_objetive->getPosition().x <= _objLastPos.x){
					dif = _objetive->getPosition().z - _nave->getPosition().z;

					if(abs(dif) > 1){
						if(dif < 0) {
				            if(_nave->getPosition().z > -11.10)
				                dir = _mright;
				        } else {
				            if(_nave->getPosition().z < 11.10)
				                dir = _mleft;
				        }//Fin if-else
				    }//Fin if
			    } else {
			    	_objetive = 0;
			    }//Fin if-else
				break;
			case N_X:
				/* Si el objetivo se acerca nos movemos */
				if(_objetive->getPosition().x >= _objLastPos.x){
					dif = _objetive->getPosition().z - _nave->getPosition().z;

					if(abs(dif) > 1){
						if(dif > 0) {
				            if(_nave->getPosition().z < 11.10)
				                dir = _mleft;
				        } else {
				            if(_nave->getPosition().z > -11.10)
				                dir = _mright;
				        }//Fin if-else
				    }//Fin if
			    } else {
			    	_objetive = 0;
			    }//Fin if-else
				break;
			case P_Z:
				/* Si el objetivo se acerca nos movemos */
				if(_objetive->getPosition().z <= _objLastPos.z){
					dif = _objetive->getPosition().x - _nave->getPosition().x;

					if(abs(dif) > 1){
						if(dif > 0) {
				            if(_nave->getPosition().x < 11.33)
				                dir = _mright;
				        } else {
				            if(_nave->getPosition().x > -11.31)
				                dir = _mleft;
				        }//Fin if-else
				    }//Fin if
			    } else {
			    	_objetive = 0;
			    }//Fin if-else
				break;
			case N_Z:
				/* Si el objetivo se acerca nos movemos */
				if(_objetive->getPosition().z >= _objLastPos.z){
					dif = _objetive->getPosition().x - _nave->getPosition().x;

					if(abs(dif) > 1){
				        if(dif > 0) {
				            if(_nave->getPosition().x < 11.33)
				                dir = _mright;
				        } else {
				            if(_nave->getPosition().x > -11.31)
				                dir = _mleft;
				        }//Fin if-else
				    }//Fin if
			    } else {
			    	_objetive = 0;
			    }//Fin if-else
				break;
		}//Fin switch

		if(dir != Ogre::Vector3::ZERO){
			_rbNave->enableActiveState();
			_nave->translate(dir * deltaT * NAV_VEL);

			_objLastPos = _objetive->getPosition();
		}//Fin if
	} else {
		_objetive = 0;
	}//Fin if-else
}//Fin setImpulso

bool Character::thinkIn(OgreBulletDynamics::RigidBody* ball){
	bool result = false;

	Ogre::Real dist = _center.distance(
		ball->getSceneNode()->getPosition());

	if(abs(dist) <= MIN_DIST){
		result = true;
	}//Fin if

	return result;
}//Fin thinkIn

Ogre::SceneNode* Character::getPix(){
	return _pix;
}//Fin getPix

Ogre::SceneNode* Character::getNave(){
	return _nave;
}//Fin getNave

OgreBulletDynamics::RigidBody* Character::getRigidBody(){
	return _rbNave;
}//Fin getNave

void Character::updateAnimation(Ogre::Real deltaT){
	/* Actualizamos animacion */
	if(_animation != -1){
		switch(_animation){
			case ROTAR:
				_animRotar->addTime(deltaT);
			break;
			case GANAR:
				_animGanar->addTime(deltaT);
			break;
			case PERDER:
				_animPerder->addTime(deltaT);
			break;

		}//Fin switch
	}//Fin if

	if(_animation == GANAR && _animGanar->hasEnded()){
		setAnimation(ROTAR);


	} else if(_animation == PERDER && _animPerder->hasEnded()){
		setAnimation(ROTAR);

	}//Fin if-else

}//Fin updateAnimation

void Character::setAnimation(int animation){
	Ogre::AnimationState *anim;

	/*Evita cortar la animacion ganar. Solo va a cambiar a rotar
	 * cuando la animacion de ganar haya terminado. Evito que sea
	 * perder y ganar la que cambien*/
	if(_animation != GANAR || animation == ROTAR){

		/* Limpiamos animaciones */
		if(_animation != -1){
			switch(_animation){
			case ROTAR:
				anim = _animRotar;
				break;
			case GANAR:
				anim = _animGanar;
				break;
			case PERDER:
				anim = _animPerder;
				break;
			}//Fin switch

			anim->setEnabled(false);
			anim->setTimePosition(0);
			anim->setLoop(false);
		}//Fin if

		_animation = animation;

		/* Activamos animaciones */
		switch(_animation){
		case ROTAR:
			anim = _animRotar;
			break;
		case GANAR:
			anim = _animGanar;
			anim->setLoop(false);
			break;
		case PERDER:
			anim = _animPerder;
			anim->setLoop(false);
			break;
		}//Fin switch

		anim->setEnabled(true);
		anim->setTimePosition(0);

		if(_animation == ROTAR){
			anim->setLoop(true);
		}//Fin if
	}//Fin if
}//Fin setAnimation

