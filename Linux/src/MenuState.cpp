#include "MenuState.h"
#include "PlayState.h"
#include "RecordManager.h"
#include "LevelSelectionState.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>


template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void MenuState::enter (){
    _root = Ogre::Root::getSingletonPtr();
    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera); 

    _nodos.reserve(NUM_NODOS);

    _state=_carasstate=ROTAR;

    _mostrarAbout = _mostrarHelp = false;

    _animaciones.reserve(5);

    if(_render){
    	renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    }//Fin if

    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
    CEGUI::System::getSingleton().setDefaultFont("American-Captain");

    CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

    if(_render){
    	createGUI();
    	_render = false;
    }//Fin if

    createScene();
    createOverlay();

   	CEGUI::MouseCursor::getSingleton().show();
    attachButtons();

    mostrarBotones();

    _exitGame = false;
}//Fin enter

void MenuState::createScene(){
	/* Variables que vamos a usar */
	Ogre::Entity *ent = NULL;
	Ogre::SceneNode *node = NULL;
	Ogre::ParticleSystem* smoke, *smoke2;

	_camera->setPosition(Ogre::Vector3(-10, 5, 85));
	_camera->lookAt(Ogre::Vector3(3, 0, 5));
	_camera->yaw( Ogre::Degree( 10 ) );

	/* Sombras */
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	_sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

	/* Iluminacion */
	node = _sceneMgr->createSceneNode("LightingNodeMenu");
	Ogre::Light *light = _sceneMgr->createLight("LightMenu");
	light->setType(Ogre::Light::LT_DIRECTIONAL);
	light->setPosition(Ogre::Vector3(-10, 10.25, 57));
	light->setDirection(Ogre::Vector3(-1, -1, 0));

	node->attachObject(light);
	_nodos.push_back(node);

	/* EARTH */
    _earth = _sceneMgr->getRootSceneNode()->
        createChildSceneNode("Earth");
    ent = _sceneMgr->createEntity("Earth.mesh");
    _earth->attachObject(ent);
    _earth->setPosition(Ogre::Vector3(-10, 2, 0));
    _earth->scale(25, 25, 25);

    /* MOON */
    _moon = _earth->createChildSceneNode("MoonNode");
    _moon->pitch(Ogre::Degree(-10));

    node = _moon->createChildSceneNode("Moon");
    ent = _sceneMgr->createEntity("Moon.mesh");
    ent->setCastShadows(false);
    node->attachObject(ent);
    node->setPosition(Ogre::Vector3(1, 0.40, 0));
    node->scale(0.5, 0.5, 0.5);

	/* Background */
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
	material->getTechnique(0)->getPass(0)->createTextureUnitState("stars.jpg");
	material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
	material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
	material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

	// Create background rectangle covering the whole screen
	_rect = new Ogre::Rectangle2D(true);
	_rect->setCorners(-1.0, 1.0, 1.0, -1.0);
	_rect->setMaterial("Background");

	// Render the background before everything else
	_rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

	// Use infinite AAB to always stay visible
	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	_rect->setBoundingBox(aabInf);

	// Attach background to the scene
	node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Background");
	node->attachObject(_rect);

	_nodos.push_back(node);

	/*Pix*/
	_Pix = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"PixMenu" , Ogre::Vector3(-10, 1, 70));
	ent = _sceneMgr->createEntity("Pix.mesh");
	ent->setMaterialName("PixMaterial0");
	ent->setCastShadows(true);
	_Pix->attachObject(ent);
	_Pix->setVisible(true);

	_smoke = _sceneMgr->createParticleSystem("Ps","flame");
	_smoke->getEmitter(0)->setEnabled(true);
	_Pix->attachObject(_smoke);

	_nodos.push_back(node);

	_animPixRotar = ent->getAnimationState("ReposoRotar");
	_animPixRotar->setEnabled(true);
	_animPixRotar->setTimePosition(0);
	_animPixRotar->setLoop(false);
	_animaciones.push_back(_animPixRotar);

	_animPixSaludar = ent->getAnimationState("Saludar");
	_animPixSaludar->setEnabled(false);
	_animPixSaludar->setTimePosition(0);
	_animPixSaludar->setLoop(false);
	_animaciones.push_back(_animPixSaludar);

	/*Pix Julian*/
	_PixJulian = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"Julian" , Ogre::Vector3(-25.8, 1, 70));
	ent = _sceneMgr->createEntity("PixCaras.mesh");
	ent->setCastShadows(true);
	_PixJulian->attachObject(ent);
	_PixJulian->setVisible(true);

	smoke = _sceneMgr->createParticleSystem("PsJulian","flame");
	smoke->getEmitter(0)->setEnabled(true);
	_PixJulian->attachObject(smoke);
	smoke->getEmitter(0)->setParameter("colour_range_start", "0.0 1.0 0.0");
	smoke->getEmitter(0)->setParameter("colour_range_end", "0.0 1.0 0.0");

	_animPixJulianRotar = ent->getAnimationState("ReposoRotar");
	_animPixJulianRotar->setEnabled(true);
	_animPixJulianRotar->setTimePosition(0);
	_animPixJulianRotar->setLoop(false);
	_animaciones.push_back(_animPixJulianRotar);

	_animPixJulianSaludar = ent->getAnimationState("Saludar");
	_animPixJulianSaludar->setEnabled(false);
	_animPixJulianSaludar->setTimePosition(0);
	_animPixJulianSaludar->setLoop(false);
	_animaciones.push_back(_animPixJulianSaludar);

	_animPixJulianAndar = ent->getAnimationState("Andar");
	_animPixJulianAndar->setEnabled(false);
	_animPixJulianAndar->setTimePosition(0);
	_animPixJulianAndar->setLoop(false);
	_animaciones.push_back(_animPixJulianAndar);

	/* Pix JOSE */
	_PixJose = _sceneMgr->getRootSceneNode()->createChildSceneNode(
			"Jose" , Ogre::Vector3(-29.8, 1, 70));
	ent = _sceneMgr->createEntity("PixCaras.mesh");
	ent->setMaterialName("PixMaterialJose");
	ent->setCastShadows(true);
	_PixJose->attachObject(ent);
	_PixJose->setVisible(true);

	smoke2 = _sceneMgr->createParticleSystem("PsJose","flame");
	smoke2->getEmitter(0)->setEnabled(true);
	_PixJose->attachObject(smoke2);
	smoke2->getEmitter(0)->setParameter("colour_range_start", "0.0 1.0 0.0");
	smoke2->getEmitter(0)->setParameter("colour_range_end", "0.0 1.0 0.0");

	_animPixJoseRotar = ent->getAnimationState("ReposoRotar");
	_animPixJoseRotar->setEnabled(true);
	_animPixJoseRotar->setTimePosition(0);
	_animPixJoseRotar->setLoop(false);
	_animaciones.push_back(_animPixJoseRotar);

	_animPixJoseSaludar = ent->getAnimationState("Saludar");
	_animPixJoseSaludar->setEnabled(false);
	_animPixJoseSaludar->setTimePosition(0);
	_animPixJoseSaludar->setLoop(false);
	_animaciones.push_back(_animPixJoseSaludar);

	_animPixJoseAndar = ent->getAnimationState("Andar");
	_animPixJoseAndar->setEnabled(false);
	_animPixJoseAndar->setTimePosition(0);
	_animPixJoseAndar->setLoop(false);
	_animaciones.push_back(_animPixJoseAndar);

	//node->yaw(Ogre::Degree(150));
	_nodos.push_back(node);

	//node->yaw(Ogre::Degree(150));
	_nodos.push_back(node);
}//Fin createScene

void MenuState::createOverlay(){
    _overlayManager = Ogre::OverlayManager::getSingletonPtr();
    _ovMenu = _overlayManager->getByName("Menu");

    _ovAbout = _overlayManager->getOverlayElement("aboutText");
    _ovHelp = _overlayManager->getOverlayElement("imgHelp");

    _ovAbout->hide();
    _ovHelp->hide();
    _ovMenu->hide();
}//Fin createOverlay

void MenuState::exit (){
	ocultarBotones();
	CEGUI::MouseCursor::getSingleton().hide();

	_ovAbout->hide();
    _ovHelp->hide();
    _ovMenu->hide();

	_nodos.clear();
	_animaciones.clear();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}//Fin exit

void MenuState::pause (){
    ocultarBotones();

    CEGUI::MouseCursor::getSingleton().hide();
}//Fin pause

void MenuState::resume (){
    attachButtons();
    mostrarBotones();
    for(int i=0; (unsigned) i < _nodos.size(); i++){
    	_nodos[i]->setVisible(true);
   	}
    CEGUI::MouseCursor::getSingleton().show();
}//Fin resume

bool MenuState::frameStarted(const Ogre::FrameEvent& evt){
	_moon->yaw(Ogre::Degree(evt.timeSinceLastFrame * 30));
	_earth->yaw(Ogre::Degree(evt.timeSinceLastFrame * 25));

	CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);

	updatePix(evt);

	updateCarteles(evt);

    return true;
}//Fin frameStarted

bool MenuState::frameEnded(const Ogre::FrameEvent& evt){
	if(_exitGame){
	    std::cout << "Guardando Records..." << std::endl;
	    RecordManager::getSingletonPtr()->saveRecords();
	    return false;
	}//Fin if

    return true;
}//Fin frameEnded

void MenuState::keyPressed(const OIS::KeyEvent &e){}

void MenuState::keyReleased(const OIS::KeyEvent &e){}

void MenuState::mouseMoved(const OIS::MouseEvent &e){
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}//Fin mouseMoved

void MenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonDown(convertMouseButton(id));
}//Fin mousePressed

void MenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
	CEGUI::System::getSingleton().injectMouseButtonUp(convertMouseButton(id));
}//Fin mouseReleased

MenuState* MenuState::getSingletonPtr (){ 
    return msSingleton;
}//Fin getSingletonPtr

MenuState& MenuState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}//Fin getSingleton

void MenuState::createGUI(){
    /* Sheet */
    sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Sheet");

    /* New Game Button */
      gameButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","gameButton");
      gameButton->setText("New Game");

      gameButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
      gameButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.3,0)));
      gameButton->subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&MenuState::lanzarJuego, this));

    /* Credits button */
    creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","creditsButton");
    creditsButton->setText("About");

    creditsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.4,0)));
    creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked,
        CEGUI::Event::Subscriber(&MenuState::creditsGUI,this));

    /* Help button */
    helpButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","helpButton");
    helpButton->setText("Help");
    helpButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    helpButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.5,0)));
    helpButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::helpGUI, this));

    /* Quit button */
    quitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button","QuitButton");
    quitButton->setText("Exit");
    quitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.68,0),CEGUI::UDim(0.6,0)));
    quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::quit, this));
}//Fin createGUI

void MenuState::attachButtons(){
    /* Attaching buttons */
	sheet->addChildWindow(helpButton);
    sheet->addChildWindow(quitButton);
    sheet->addChildWindow(creditsButton);
    sheet->addChildWindow(gameButton);

    CEGUI::System::getSingleton().setGUISheet(sheet);
}//Fin attachButtons

bool MenuState::quit(const CEGUI::EventArgs &e){
    _exitGame = true;
    return true;
}//Fin quit


bool MenuState::menuGUI(const CEGUI::EventArgs &e){
    /* Sheet */
    CEGUI::WindowManager::getSingleton().getWindow("Sheet");

    ocultarBotones();

    mostrarBotones();
    return true;
}//Fin menuGUI

bool MenuState::creditsGUI(const CEGUI::EventArgs &e){

	Ogre::Vector3 vn(1, 0, 0);
	Ogre::Vector3 vnneg(-1, 0, 0);

	//Para que se lo lleve cuando está mostrandose
	setAnimationCaras(ANDAR);

	if(_mostrarAbout){
		_mostrarAbout = false;
		_PixJulian->lookAt(vnneg , Ogre::Node::TS_WORLD, Ogre::Vector3::NEGATIVE_UNIT_X);
		_PixJose->lookAt(vnneg  , Ogre::Node::TS_WORLD, Ogre::Vector3::NEGATIVE_UNIT_X);
	}
	else{
		if(_ovHelp->isVisible()){
			_mostrarHelp = false;
			_ovHelp->hide();
			_ovMenu->hide();
		}
		_mostrarAbout = true;
		_PixJulian->lookAt(vn  , Ogre::Node::TS_WORLD, Ogre::Vector3::UNIT_X);
		_PixJose->lookAt(vn  , Ogre::Node::TS_WORLD, Ogre::Vector3::UNIT_X);
	}//Fin if-else

    return true;
}//Fin creaditsGUI

bool MenuState::helpGUI(const CEGUI::EventArgs &e){
	if(!_mostrarHelp && !_mostrarAbout){
		_mostrarHelp = true;

		_ovHelp->show();
	    _ovMenu->show();
	} else if(!_mostrarAbout){
		_mostrarHelp = false;

		_ovHelp->hide();
	    _ovMenu->hide();
	}//Fin if-else
	    
	return true;
}//Fin helpGUI

bool MenuState::lanzarJuego(const CEGUI::EventArgs &e){
	for(int i=0; (unsigned) i < _nodos.size(); i++){
		_nodos[i]->setVisible(false);
	}//Fin for

    changeState(LevelSelectionState::getSingletonPtr());

	return true;
}//Fin lanzarJuego

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id){
    CEGUI::MouseButton ceguiId;

    switch(id){
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }//Fin switch
  return ceguiId;
}//Fin convertMouseButton

void MenuState::ocultarBotones(){
    /* Attaching buttons */
    quitButton->hide();
    creditsButton->hide();
    gameButton->hide();
    helpButton->hide();
}//Fin ocultarBotones

void MenuState::mostrarBotones(){
    quitButton->show();
    creditsButton->show();
    gameButton->show();
    helpButton->show();
}//Fin mostrarBotones

void MenuState::setAnimation(int state){
	Ogre::AnimationState *anim;

	/* Limpiamos animaciones */
	if(_state != -1){
		switch(_state){
			case ROTAR:
				anim = _animPixRotar;
			break;
			case SALUDAR:
				anim = _animPixSaludar;
			break;

		}//Fin switch

		anim->setEnabled(false);
	  	anim->setTimePosition(0);
	  	anim->setLoop(false);
  	}//Fin if

	_state = state;

	/* Activamos animaciones */
	switch(_state){
		break;
		case ROTAR:
			anim = _animPixRotar;
		break;
		case SALUDAR:
			anim = _animPixSaludar;
		break;
	}//Fin switcg


	anim->setEnabled(true);
  	anim->setTimePosition(0);

  	if(_state != ROTAR && _state != SALUDAR){
  		anim->setLoop(true);
  	}//Fin if
}//Fin setAnimation

void MenuState::updatePix(const Ogre::FrameEvent& evt){

	Ogre::Vector3 vn(1, 0, 0);
	Ogre::Vector3 vnneg(-1, 0, 0);

	//Cuando se encuentra en un estado tiene se desplaza hacia un sentido u otro
	//y mira hacia un lado concreto

	switch(_state){
		case ANDAR:
		case ROTAR:
			_animPixRotar->addTime(evt.timeSinceLastFrame);
			break;
		case SALUDAR:
			_animPixSaludar->addTime(evt.timeSinceLastFrame);
			break;
	}//Fin swicth

	if(_animPixSaludar->hasEnded()){
		setAnimation(ROTAR);

	}//Fin if

	if(_animPixRotar->hasEnded()){
		setAnimation(SALUDAR);
	}//Fin if
}//Fin updatePix

/*
 * Se aplica el movimiento a los carteles cuando se pulsa el botón
 * correspondiente. Además espera al estado de TIRAR o EMPUJAR de Pix
 * para moverse en el sentido correspondiente
 */
void MenuState::updateCarteles(const Ogre::FrameEvent& evt){

	Ogre::Vector3 vn(1, 0, 0);
	Ogre::Vector3 vnneg(-1, 0, 0);
	Ogre::Vector3 vnz(0,0,-1);

	switch(_carasstate){

	case ANDAR:
		//mostrar About

		if(_mostrarAbout){

			if(_PixJulian->getPosition().x < -14){
			_PixJulian->translate(vn  * evt.timeSinceLastFrame * 1.5 );
			_PixJose->translate(vn  * evt.timeSinceLastFrame * 1.5 );
			}else {
				setAnimationCaras(ROTAR);
				_PixJulian->lookAt(vnz , Ogre::Node::TS_WORLD, Ogre::Vector3::NEGATIVE_UNIT_Z);
				_PixJose->lookAt(vnz  , Ogre::Node::TS_WORLD, Ogre::Vector3::NEGATIVE_UNIT_Z);
				if(!_ovMenu->isVisible()){
					_ovMenu->show();
					_ovAbout->show();
				}//Fin if
			}//Fin if-else
		}
		else{
			if(_PixJulian->getPosition().x > -25.8){
				if(_ovAbout->isVisible()){
					_ovMenu->hide();
					_ovAbout->hide();
				}//Fin if
				_PixJulian->translate(vnneg  * evt.timeSinceLastFrame * 1.5 );
				_PixJose->translate(vnneg  * evt.timeSinceLastFrame * 1.5 );
			}
			else {
				setAnimationCaras(ROTAR);
			}//Fin if-else
		}//Fin if-else

		_animPixJulianAndar->addTime(evt.timeSinceLastFrame);
		_animPixJoseAndar->addTime(evt.timeSinceLastFrame);

		break;
	case ROTAR:
		_animPixJulianRotar->addTime(evt.timeSinceLastFrame);
		_animPixJoseRotar->addTime(evt.timeSinceLastFrame);
		break;
	case SALUDAR:
		_animPixJulianSaludar->addTime(evt.timeSinceLastFrame);
		_animPixJoseSaludar->addTime(evt.timeSinceLastFrame);
		break;
	}//Fin switch

	if(_animPixJulianSaludar->hasEnded()){
		setAnimationCaras(ROTAR);
	}//Fin if

	if(_animPixJulianRotar->hasEnded()){
		setAnimationCaras(SALUDAR);
	}//Fin if
}//Fin updateCarteles

void MenuState::setAnimationCaras(int state){
	Ogre::AnimationState *anim, *anim2;

		/* Limpiamos animaciones */
		if(_carasstate != -1){
			switch(_carasstate){
				case ANDAR:
					anim = _animPixJulianAndar;
					anim2 = _animPixJoseAndar;
					break;
				case ROTAR:
					anim = _animPixJulianRotar;
					anim2 = _animPixJoseRotar;
				break;
				case SALUDAR:
					anim = _animPixJulianSaludar;
					anim2 = _animPixJoseSaludar;
				break;
			}//Fin switch

			anim->setEnabled(false);
		  	anim->setTimePosition(0);
		  	anim->setLoop(false);

		  	anim2->setEnabled(false);
		  	anim2->setTimePosition(0);
		  	anim2->setLoop(false);

	  	}//Fin if

		_carasstate = state;

		/* Activamos animaciones */
		switch(_carasstate){
		case ANDAR:
			anim=_animPixJulianAndar;
			anim2=_animPixJoseAndar;
			break;
		case ROTAR:
			anim = _animPixJulianRotar;
			anim2 = _animPixJoseRotar;
			break;
		case SALUDAR:
			anim = _animPixJulianSaludar;
			anim2 = _animPixJoseSaludar;
			break;
		}//Fin switch

		anim->setEnabled(true);
	  	anim->setTimePosition(0);
		anim2->setEnabled(true);
		anim2->setTimePosition(0);

	  	if(_carasstate != ROTAR && _carasstate != SALUDAR){
	  		anim2->setLoop(true);
	  		anim->setLoop(true);
	  	}//Fin if
}//Fin setAnimationCaras
