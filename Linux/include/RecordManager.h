#ifndef RecordManager_h
#define RecordManager_h

#include <map>
#include <string>

#include <OgreString.h>
#include <OgreStringConverter.h>
#include <OgreSingleton.h>

#include "Record.h"

class RecordManager: public Ogre::Singleton<RecordManager>{
public:
	RecordManager();
	~RecordManager();

	void saveRecords();
	void loadRecords();

	bool hasRecord(int planet);
	
	void addRecord(Record rec);
	Record getRecord(int planet);

	/* Heredados de Ogre::Singleton. */
    static RecordManager& getSingleton ();
    static RecordManager* getSingletonPtr ();
private:
	std::string _docName;
	std::map <int, Record> _records;
};//Fin RecordManager

#endif