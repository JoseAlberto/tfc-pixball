#ifndef Record_h
#define Record_h

#include <Ogre.h>

class Record{
public:
	Record();
	Record(int planet, Ogre::Real time, int points);

	void setPoints(int po);
	void setPlanet(int planet);
	void setTime(Ogre::Real t);

	int getPoints() const;
	int getPlanet() const;
	Ogre::Real getTime() const;

	Ogre::String toString();
private:
	Ogre::Real _time;
	int _points, _planet;
};

#endif