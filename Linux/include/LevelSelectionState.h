#ifndef LevelSelectionState_H
#define LevelSelectionState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

#define MAX_PLANETS 4

#define MERCURY 0
#define VENUS   1
#define EARTH   2
#define MARS    3

class LevelSelectionState : public Ogre::Singleton<LevelSelectionState>, public GameState {
    public:
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        static LevelSelectionState& getSingleton ();
        static LevelSelectionState* getSingletonPtr ();

    protected:
        void createScene();
        void createOverlay();

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        Ogre::Overlay *_ovLevels, *_ovMercury, *_ovVenus, *_ovEarth, *_ovMars;
        Ogre::OverlayElement *_o_score, *_o_powerUps;
        Ogre::OverlayElement *_o_name, *_o_dleft, *_o_dright;

        /* Background */
        Ogre::Rectangle2D* _rect;

        /* Planets */
        Ogre::SceneNode* _moon;
        std::vector<Ogre::SceneNode*> _planets;

        /* Move Camera Vector */
        Ogre::Vector3 _dir;

        int _selection;
        bool _exitGame, _moveCamera;
};

#endif