#ifndef IAMANAGER_H
#define IAMANAGER_H

#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>

#include <vector>

#include "Player.h"
#include "Ball.h"

class IAManager : public Ogre::Singleton<IAManager>{
public:
	~IAManager();

	void start();
	void update(Ogre::Real deltaT);
	void updateIAS();
	void reset();

	void setPlayers(std::vector<Player*> js);

	void addFreeBall(Ball* ball);
	void updateBalls();
	void updateVelocidad();
	void updatePlayers();


	int getMode();
	int getNumActBalls();
	Ball* getBooster(Ogre::SceneNode* node);

	static IAManager& getSingleton ();
    static IAManager* getSingletonPtr ();
private:
	Ogre::Real _deltaT;
	int _mode, _numActBalls;

	Ogre::SceneManager* _sceneMgr;

	std::vector<Player*> _players;

	std::vector<Ball*> _balls;
};

#endif
