#ifndef MenuState_H
#define MenuState_H

#include <vector>
#include <Ogre.h>
#include <OIS/OIS.h>

#include <CEGUI.h>
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "GameState.h"
#define NUM_NODOS 23

#define TIRAR 0
#define EMPUJAR 1
#define ROTAR 2
#define SALUDAR 3
#define ANDAR 4

class MenuState : public Ogre::Singleton<MenuState>, public GameState{
    public:
       MenuState() {
            _render = true; 
        }

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        
        void createGUI();
        void createScene();
        void createOverlay();

        bool quit(const CEGUI::EventArgs &e);
        bool menuGUI(const CEGUI::EventArgs &e);
        bool creditsGUI(const CEGUI::EventArgs &e);
        bool helpGUI(const CEGUI::EventArgs &e);

        bool lanzarJuego(const CEGUI::EventArgs &e);

        void ocultarBotones();
        void mostrarBotones();
        void attachButtons();
        void setAnimation(int state);
        void setAnimationCaras(int state);
        void updatePix(const Ogre::FrameEvent& evt);
        void updateCarteles(const Ogre::FrameEvent& evt);

        CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

        /* Heredados de Ogre::Singleton. */
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        Ogre::SceneNode  *_nodBase;

        float _timeSinceLastFrame;
        CEGUI::OgreRenderer* renderer;    
        CEGUI::Window* sheet;
        CEGUI::Window* quitButton;
        CEGUI::Window* creditsButton;
        CEGUI::Window* gameButton;
        CEGUI::Window* helpButton;

        Ogre::Overlay *_ovMenu;
        Ogre::OverlayElement *_ovAbout, *_ovHelp;

        Ogre::AnimationState* _animPixRotar,* _animPixSaludar,
       * _animPixJulianAndar,* _animPixJulianRotar, *_animPixJulianSaludar,
       * _animPixJoseAndar,* _animPixJoseRotar, *_animPixJoseSaludar;

        std::vector<Ogre::SceneNode*> _nodos;

        std::vector<Ogre::AnimationState*> _animaciones;

        int _state, _carasstate;
        bool _mostrarAbout, _mostrarHelp;

        Ogre::SceneNode* _Pix, *_Records, *_PixJulian, *_PixJose, *_earth, *_moon;
        Ogre::ParticleSystem* _smoke;

        /* Background */
        Ogre::Rectangle2D* _rect;

        bool _exitGame;
        bool _render;

};

#endif
