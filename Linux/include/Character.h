#ifndef CHARACTER
#define CHARACTER

#include <vector>
#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>

#define NAV_VEL  20
#define RIGHT     0
#define LEFT      1
#define P_X       2
#define P_Z       3
#define N_X       4
#define N_Z       5
#define MIN_DIST 10




class Character{
public:
	Character(int type, Ogre::Vector3 left, Ogre::Vector3 right, Ogre::SceneNode* pix, 
		Ogre::SceneNode* nave, Ogre::Entity *ent);

	void setPix(Ogre::SceneNode* nodo);
	void setNave(Ogre::SceneNode* nodo);
	void setRigidBody(OgreBulletDynamics::RigidBody* rb);
	void setObjetive(Ogre::SceneNode* ball);

	bool hasObjetive();
	void addObjetive(OgreBulletDynamics::RigidBody* ball);
	bool thinkIn(OgreBulletDynamics::RigidBody* ball);

	void move(int flag, Ogre::Real deltaT);
	void moveToCenter(Ogre::Real deltaT);
	void moveToObjetive(Ogre::Real deltaT);

	void updateAnimation(Ogre::Real deltaT);
	void setAnimation(int animation);

	Ogre::SceneNode* getPix();
	Ogre::SceneNode* getNave();
	OgreBulletDynamics::RigidBody* getRigidBody();

private:
	int _type;

	Ogre::Vector3 _center, _mleft, _mright, _objLastPos;

	Ogre::SceneNode* _pix;
	Ogre::SceneNode* _nave;
	Ogre::SceneNode* _objetive;
	OgreBulletDynamics::RigidBody* _rbNave;

	Ogre::AnimationState *_animRotar, *_animGanar, *_animPerder;

	int _animation;
};

#endif
