#ifndef BALL_H_
#define BALL_H_

#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>

class Ball {
private:
	bool _active, _sonido;
	OgreBulletDynamics::RigidBody* _rigidBall;
	Ogre::SceneNode* _nodeBall;
	int _tipo;

public:
	Ball(bool active, OgreBulletDynamics::RigidBody * rigidBody,
			Ogre::SceneNode* sceneNode, int tipo);
	void setActive(int active);
	void setRigidBody( OgreBulletDynamics::RigidBody * rigidBody);
	void setSceneNode( Ogre::SceneNode* sceneNode);
	void setTipo(int tipo);
	void setSonar(bool sonar);

	Ogre::SceneNode* getSceneNode();
	OgreBulletDynamics::RigidBody* getRigidBody();
	bool getActive();
	int getTipo();
	bool getSonar();
};

#endif
