#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <vector>
#include <string>
#include <OIS/OIS.h>

#include <OgreBulletDynamicsRigidBody.h>

#include <Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <Shapes/OgreBulletCollisionsSphereShape.h>
#include <Shapes/OgreBulletCollisionsCylinderShape.h>

#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h> 

#include "GameState.h"
#include "IAManager.h"
#include "Character.h"
#include "PauseState.h"
#include "EndState.h"
#include "MenuState.h"
#include "Player.h"
#include "Ball.h"
#include "TrackManager.h"
#include "SoundFXManager.h"

#define MAX_PLAYERS 4
#define MAX_GENERADORES 4
#define MAX_BALLS 7
#define MIN_TIME 1.5

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
    public:
        PlayState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void setLevel(int level);
        void endGame();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

    protected:
        void createScene();
        void createOverlay();
        void createWorld();
        void shootBall();
        void updatePlayer();
        void updateOverlays();
        void DetectCollisionDrain();
        void activateWall(int idx);
        void updatePlayerEffects();

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        OgreBulletDynamics::DynamicsWorld* _world;
        OgreBulletCollisions::DebugDrawer* _debugDrawer;

        /* Overlays */
        Ogre::OverlayElement *_ovTime;
        Ogre::Overlay *_ovPlay, *_ovBlue, *_ovRed, *_ovYellow, *_ovGreen;

        std::vector<Ogre::OverlayElement*> _bFaces;
        std::vector<Ogre::OverlayElement*> _rFaces;
        std::vector<Ogre::OverlayElement*> _yFaces;
        std::vector<Ogre::OverlayElement*> _gFaces;
        std::vector<Ogre::OverlayElement*> _points;

        /* Cuerpos Naves */
        std::deque <OgreBulletDynamics::RigidBody *>         _navBodies;
        /* Cuerpos Generadores */
        std::deque <OgreBulletDynamics::RigidBody *>        _genBodies;
        /* Cuerpos Barreras*/
        std::deque <OgreBulletDynamics::RigidBody *>        _wallBodies;
        /* Cuerpos Goals */
        std::deque <OgreBulletDynamics::RigidBody *>        _goalBodies;

        /* Shapes */
        std::deque <OgreBulletCollisions::CollisionShape *>  _shapes;

        /* Cuerpo plataforma */
        OgreBulletDynamics::RigidBody *_rbStage;

        /* Background */
        Ogre::Rectangle2D* _rect;   

        bool _endGame, _exitGame, _lPress, _rPress, _firstperson;

        int _numBalls, _id, _level, _outPlayers;

        Ogre::Real _deltaT, _time, _timeLastShot;

        std::vector<Player*> _players;
        std::vector<Ogre::SceneNode*> _generadores, _iceBlocks, _fireBlocks;
        std::vector<Ogre::SceneNode*> _goals;

        /*Sonidos*/
        TrackManager* _pTrackManager;
        SoundFXManager* _pSoundFXManager;

        SoundFXPtr _choqueMetal, _velocidad, _congelar, _vida;
};

#endif
