#ifndef EndState_H
#define EndState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "Record.h"
#include "Player.h"
#include "GameState.h"

#define LOSE 0
#define WIN  1

class EndState : public Ogre::Singleton<EndState>, public GameState{
    public:
        EndState() {};

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void result(int flag);
        void result(int flag, Player* pj, int planet, Ogre::Real time);

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        static EndState& getSingleton ();
        static EndState* getSingletonPtr ();

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        
    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        Ogre::Overlay *_ovLose, *_ovWin;
        Ogre::OverlayElement *_ovExit, *_ovPoints;
        
    private:
        void createOverlay();

        bool _endGame;
        Player* _winner;
        Ogre::Real _time;
        int _mode, _selection;

        /*Sonidos*/
        TrackManager* _pTrackManager;
        SoundFXManager* _pSoundFXManager;

        SoundFXPtr _ganar, _perder;
};

#endif
