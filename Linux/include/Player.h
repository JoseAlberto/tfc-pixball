#ifndef PLAYER
#define PLAYER

#include <Character.h>
#include "TrackManager.h"
#include "SoundFXManager.h"

#define P_INIT  15

#define T_IA    0
#define T_REAL  1

class Player{
public:
	Player(std::string nombre, int tipo);
	~Player();

	int getTipo();
	int getPuntuacion();
	bool lose();
	std::string getNombre();
	Character* getCharacter();
	int getEstado();
	Ogre::Real getTime();

	void setTipo(int tipo);
	void setCharacter(Character* pj);
	void lose(bool lose);
	void setNombre(std::string nombre);
	void setPuntuacion(int puntuacion);
	void setEstado(int estado);
	void setTime(Ogre::Real deltaT);

	void decreasePoints();
private:
	int _tipo;
	int _puntuacion;
	bool _lose;
	std::string _nombre;
	int _estado; //Indica el booster que ha cogido
	Character* _pj;
	Ogre::Real _timeBooster;
	
	/*Sonidos*/
	TrackManager* _pTrackManager;
	SoundFXManager* _pSoundFXManager;

	SoundFXPtr _choqueMetal, _velocidad, _congelar, _vida;

};

#endif
