#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class IntroState : public Ogre::Singleton<IntroState>, public GameState {
    public:
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        static IntroState& getSingleton ();
        static IntroState* getSingletonPtr ();

    protected:
        void createScene();
        void createOverlay();

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;
        Ogre::OverlayManager* _overlayManager;

        Ogre::Overlay *_ovintro;
        Ogre::OverlayElement *_ov_bullet, *_ov_ogre;

        Ogre::Rectangle2D* _rect;

        bool _exitGame, _scene;

        Ogre::Real _time;

        TrackManager* _pTrackManager;
        TrackPtr _mainTrack;

};

#endif
